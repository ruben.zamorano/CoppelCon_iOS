//
//  AppDelegate.swift
//  CoppelCom
//
//  Created by MacBook Pro on 16/11/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import UIKit
import ZDCChat
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import Fabric
import Crashlytics
import CoreData
import Firebase
import MaterialComponents.MaterialSnackbar

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var bandera = false

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        sleep(3)
        
        Fabric.with([Crashlytics.self])

        //self.handlePushNotificationWithUserInfo(launchOptions: launchOptions)
        
        ZDCChat.initialize(withAccountKey: "NjzxNsZhwOJ3GWHltmV3jttBfK91b0iZ")
        
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true

        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        let sessionUser = UserDefaults.standard
        
        if let id = sessionUser.object(forKey: JSONKey.KEY_ID_USER) as? String{
            
            print(id)
            let viewController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "convencionController") as! UINavigationController
            self.window?.rootViewController = viewController
            
        }else{
            
            print("Vacio")
        }
        
        return true
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        bandera = false
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("ENTROOOOOOO")
        UIApplication.shared.applicationIconBadgeNumber = 0
        bandera = true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    
    
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])
    {
        
        if let messageID = userInfo["aps"] {
            print("Message 1 ID: \(messageID)")
        }
        
        print(userInfo)
    }
    
    var badgeCount = 0
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        
        
        if let messageID = userInfo["aps"] as? NSDictionary {
            print("Message ID: \(messageID)")
            
                badgeCount += 1
                UIApplication.shared.applicationIconBadgeNumber = badgeCount
            if let alert = messageID.value(forKey: "alert") as? NSDictionary{
                
                        let contx = self.persistentContainer.viewContext
                        let newNot = NSEntityDescription.insertNewObject(forEntityName: "Notify", into: contx)
                        //newNot.setValue( messageID.value(forKey: "alert") , forKey: JSONKey.KEY_MSG)
                
                
                        newNot.setValue(alert.value(forKey: "title"), forKey: JSONKey.KEY_TITLE)
                        newNot.setValue(false, forKey: JSONKey.KEY_STATUS)
                        newNot.setValue(alert.value(forKey: "body"), forKey: JSONKey.KEY_MSG)
                        newNot.setValue("cero", forKey: JSONKey.KEY_ACTION)
                
                
                    do{
                        
                        try contx.save()
                        print("SAVED")
                        
                    }catch{
                        
                        print("NO SAVED")
                        
                    }
                
                let message = MDCSnackbarMessage()
                message.text = "Tienes una notificacion nueva, ve al apartado de noficicaciones para verla."
                MDCSnackbarManager.show(message)
                
                
            }else{
                
   
                
                let contx = self.persistentContainer.viewContext
                let newNot = NSEntityDescription.insertNewObject(forEntityName: "Notify", into: contx)
                
                newNot.setValue(messageID.value(forKey: "alert"), forKey: JSONKey.KEY_TITLE)
                newNot.setValue(false, forKey: JSONKey.KEY_STATUS)
                newNot.setValue("Notificacion desde Firebase", forKey: JSONKey.KEY_MSG)
                newNot.setValue("cero", forKey: JSONKey.KEY_ACTION)
                
                
                do{
                    
                    try contx.save()
                    print("SAVED")
                    
                }catch{
                    
                    print("NO SAVED")
                    
                }
                
                
                let message = MDCSnackbarMessage()
                message.text = "Tienes una notificacion nueva, ve al apartado de noficicaciones para verla."
                MDCSnackbarManager.show(message)
                
            }
            
            
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    
    func closeWindow(){
        
        
        self.window?.rootViewController?.dismiss(animated: true, completion: nil)
        window?.rootViewController?.dismiss(animated: true, completion: nil)
        
    }
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
       
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
               
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private func createRecordForEntity(_ entity: String, inManagedObjectContext managedObjectContext: NSManagedObjectContext) -> NSManagedObject? {
        // Helpers
        var result: NSManagedObject?
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entity, in: managedObjectContext)
        
        if let entityDescription = entityDescription {
            // Create Managed Object
            result = NSManagedObject(entity: entityDescription, insertInto: managedObjectContext)
        }
        
        return result
    }
   
}

