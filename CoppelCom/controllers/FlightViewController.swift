//
//  FlightViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 11/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MaterialSnackbar

class FlightViewController: UIViewController {

    var ref: DatabaseReference = Database.database().reference()
    var arrayFlightsOut:[FlightsOut] = []
    var arrayFlightsIn:[FlightsOut] = []
    

    //@IBOutlet weak var tvSubTrans: UITableView!
    @IBOutlet weak var lblNombreEmpleado: UILabel!
    @IBOutlet weak var lblWifeName: UILabel!
    @IBOutlet weak var tvFlightsOut: UITableView!
    
    @IBOutlet weak var tvFlightsIn: UITableView!
    @IBOutlet weak var segmentedController: UISegmentedControl!
    
    @IBOutlet weak var fragmentOne: UIView!
    @IBOutlet weak var fragmentTwo: UIView!
    
    
    let sessionUser = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Itinerario"
        if !Reachability.isConnectedToNetwork(){
            
            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            
        }
        
        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        let idUser = sessionUser.object(forKey: JSONKey.KEY_ID_USER) as! String
        
        ref.child(con).child("usuarios").child(idUser).observe( .value, with: { snapshat in
            
            if let JSON = snapshat.value as? NSDictionary{
                
                let hotel = JSON.value(forKey: "transportes") as? NSDictionary
                let nombreEmpleado = self.sessionUser.object(forKey: JSONKey.KEY_C_NOMBRE_EMPLEADO) as? String ?? ""
                
                
                self.lblNombreEmpleado.text = nombreEmpleado
                self.lblWifeName.text = self.sessionUser.object(forKey: JSONKey.KEY_B_PAREJA) as? String ?? "Sin pareja"
                
                self.arrayFlightsOut.removeAll()
                self.arrayFlightsIn.removeAll()
                
                
                if let array = hotel?.value(forKey: "lstSalida") as? NSArray{
                
                
                        for i in array{
                            
                            let vObject = i as? NSDictionary
                            
                            let vuelo = FlightsOut()
                            vuelo.descipcion = vObject?.value(forKey: JSONKey.KEY_C_DESCRIPCION) as? String ?? ""
                            vuelo.fecha = vObject?.value(forKey: "cFecha") as? String ?? ""
                            
                            vuelo.horaLlegada = vObject?.value(forKey: "cHoraLlegada") as? String ?? ""
                            vuelo.horaSalida = vObject?.value(forKey: "cHoraSalida") as? String ?? ""
                            vuelo.linea = vObject?.value(forKey: "cLineaTransporte") as? String ?? ""
                            vuelo.tipo = vObject?.value(forKey: "nIdTipoTransporte") as? String ?? ""
                            
                            self.arrayFlightsOut.append(vuelo)
                            
                            print("----------------")
                            print(vuelo.descipcion)
                            print(vuelo.fecha)
                            print(vuelo.horaLlegada)
                            print(vuelo.horaSalida)
                            print(vuelo.linea)
                            print(vuelo.tipo)
                            
                            
                        }
            }
                
                
                
                if let arrayIn = hotel?.value(forKey: "lstRetorno") as? NSArray{
                        for v in arrayIn{
                            
                            let vObject = v as? NSDictionary
                            
                            let vuelo = FlightsOut()
                            vuelo.descipcion = vObject?.value(forKey: JSONKey.KEY_C_DESCRIPCION) as? String ?? ""
                            vuelo.fecha = vObject?.value(forKey: "cFecha") as? String ?? ""
                            
                            vuelo.horaLlegada = vObject?.value(forKey: "cHoraLlegada") as? String ?? ""
                            vuelo.horaSalida = vObject?.value(forKey: "cHoraSalida") as? String ?? ""
                            vuelo.linea = vObject?.value(forKey: "cLineaTransporte") as? String ?? ""
                            vuelo.tipo = vObject?.value(forKey: "nIdTipoTransporte") as? String ?? ""
                            
                            self.arrayFlightsIn.append(vuelo)
                            
                        }
                }
                        self.tvFlightsOut.reloadData()
                self.tvFlightsIn.reloadData()
                        //self.tvSubTrans.reloadData()
                
            }else{
                
                let message = MDCSnackbarMessage()
                message.text = "No tiene registros de transporte."
                MDCSnackbarManager.show(message)
                
            }
            
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        segmentedController.tintColor = UIColor.clear
        segmentedController.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black, (NSAttributedStringKey.font as NSCopying) as! AnyHashable:UIFont(name: "Helvetica Neue", size: 15.0)!], for: UIControlState.normal)
        
    }
    

    @IBAction func actionSegment(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            
            fragmentTwo.backgroundColor = UIColor.white
            fragmentOne.backgroundColor = UIColor(red: 245/255, green: 75/255, blue: 127/255, alpha: 1.0)
            
            tvFlightsOut.isHidden = false
            tvFlightsIn.isHidden = true
            
            break
        case 1:
            
            fragmentOne.backgroundColor = UIColor.white
            fragmentTwo.backgroundColor = UIColor(red: 245/255, green: 75/255, blue: 127/255, alpha: 1.0)
            
            tvFlightsOut.isHidden = true
            tvFlightsIn.isHidden = false
            
            break
        default:
            break
        }
    }
}

extension FlightViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var num = 0
        
        if tableView == tvFlightsOut {
            
            num = arrayFlightsOut.count
        }else{
            
            num = arrayFlightsIn.count
            
        }
        
        
        return num
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = CustomTableViewCell()
        
        
        
        if tableView == tvFlightsOut {
            if(arrayFlightsOut[indexPath.row].tipo == "1"){

                cell = tableView.dequeueReusableCell(withIdentifier: "cellFlightOut", for: indexPath) as! CustomTableViewCell
                
                cell.lblAreoFlight.text = arrayFlightsOut[indexPath.row].linea
                cell.lblDateFlight.text = arrayFlightsOut[indexPath.row].fecha
                cell.lblDestinoFlight.text = arrayFlightsOut[indexPath.row].descipcion
                
                let hora = "\(arrayFlightsOut[indexPath.row].horaLlegada)"
                cell.lblHourFlight.text = hora
            }else if(arrayFlightsOut[indexPath.row].tipo == "2"){
                
                cell = tableView.dequeueReusableCell(withIdentifier: "cellBusOut", for: indexPath) as! CustomTableViewCell
                
                cell.lblDateBus.text = arrayFlightsOut[indexPath.row].fecha
                cell.lblDescBus.text = arrayFlightsOut[indexPath.row].descipcion
                cell.lblLineBus.text = arrayFlightsOut[indexPath.row].linea
                //cell.lblNumberBus.text = "\(arrayFlightsOut[indexPath.row].tipo)"
                
                let hora = "\(arrayFlightsOut[indexPath.row].horaLlegada)"
                cell.lblHourBus.text = hora
                
            }else{
                
                cell = tableView.dequeueReusableCell(withIdentifier: "cellCarOut", for: indexPath) as! CustomTableViewCell
                
                cell.lblDateCar.text = arrayFlightsOut[indexPath.row].fecha
                cell.lblDestinityCar.text = "\(arrayFlightsOut[indexPath.row].descipcion)"
                cell.lblDescriptionCar.text = "\(arrayFlightsOut[indexPath.row].linea)"
                cell.lblHourCar.text = arrayFlightsOut[indexPath.row].horaLlegada
                
            }
        
        }else{
            
            //print(arrayFlightsIn[indexPath.row])
            
            if(arrayFlightsIn[indexPath.row].tipo == "1"){
                cell = tableView.dequeueReusableCell(withIdentifier: "cellFlightIn", for: indexPath) as! CustomTableViewCell
                
                cell.lblLineFlightSub.text = arrayFlightsIn[indexPath.row].linea
                cell.lblDateFlightSub.text = arrayFlightsIn[indexPath.row].fecha
                cell.lblDescFlightSub.text = arrayFlightsIn[indexPath.row].descipcion
                
                let hora = "\(arrayFlightsIn[indexPath.row].horaLlegada)"
                cell.lblHourFlightSub.text = hora
            
            }else if(arrayFlightsIn[indexPath.row].tipo == "2"){
                
                cell = tableView.dequeueReusableCell(withIdentifier: "cellBusIn", for: indexPath) as! CustomTableViewCell
                
                cell.lblDateBusSub.text = arrayFlightsIn[indexPath.row].fecha
                cell.lblDescBusSub.text = arrayFlightsIn[indexPath.row].descipcion
                cell.lblLineBusSub.text = arrayFlightsIn[indexPath.row].linea
                //cell.lblNumberBusSub.text = "\(arrayFlightsIn[indexPath.row].tipo)"
                
                let hora = "\(arrayFlightsIn[indexPath.row].horaLlegada)"
                
                cell.lblHourBusSub.text = hora
                
            }else{
                
                cell = tableView.dequeueReusableCell(withIdentifier: "cellCarIn", for: indexPath) as! CustomTableViewCell
                
                cell.lblDateCarSub.text = arrayFlightsOut[indexPath.row].fecha
                cell.lblDescCarSub.text = "\(arrayFlightsOut[indexPath.row].linea)"
                cell.lblDestiCarSub.text = "\(arrayFlightsOut[indexPath.row].descipcion)"
            }
            
        }
        
        
        
        cell.isSelected = false
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
}
