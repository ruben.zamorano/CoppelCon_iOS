//
//  GaleryViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 10/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import MaterialComponents.MaterialSnackbar

class GaleryViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {

    @IBOutlet weak var cvGalery: UICollectionView!
    
    
    var ref: DatabaseReference = Database.database().reference()
    
    let sessionUser = UserDefaults.standard
    var arrayImgs:[[UIImage]] = [[]]
    var arrayHeaders:[String] = ["Mis imagenes"]
    
    //@IBOutlet weak var bacShowImage: UIView!
    @IBOutlet weak var imgShow: UIImageView!
    
    let picker = UIImagePickerController()
    var alert = UIAlertController()
    
    
    @IBOutlet weak var bacShowImage: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(drag(_:)))
        self.bacShowImage.addGestureRecognizer(panGestureRecognizer)
        //self.bacShowImage.backgroundColor = UIColor.black
        
        
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.hole))
//        swipeRight.direction = UISwipeGestureRecognizerDirection.up
        //self.bacShowImage.addGestureRecognizer(swipeRight)
        
        alert = UIAlertController(title: "Espere un momento", message: "Obteniendo datos.", preferredStyle:.alert)
        
        
        bacShowImage.isHidden = true
        
        navigationItem.title = "Galeria de fotos"
        if !Reachability.isConnectedToNetwork(){
            
           // self.present(alert, animated: true){

            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            //}
            
        }else{
        
                //self.llamadaWeb()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        arrayImgs[0].removeAll() 
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Photos")
        request.returnsObjectsAsFaults = false
        
        do{
            
            let result = try context.fetch(request)
            
            if result.count > 0{
                
                
                
                for r in result as! [NSManagedObject]{
                    
                
                    
                    let imagen = r.value(forKey: "photo") as! String
                    
                    //print(newNot.status)
                    let convImg = ConvertBase64.DecodingBase64(image: imagen)
                    arrayImgs[0].append(UIImage(data: convImg as Data)!)
                    cvGalery.reloadData()
                }
                
                
                cvGalery.reloadData()
                
            }else{
                
                print("BD_empty[X02]")
            }
            
        }catch{
            
            print("Catch[X01]")
        }
    }
    
    
    
    
    @objc func drag(_ gesture: UIPanGestureRecognizer){
        
        let trans = gesture.translation(in: view)
        bacShowImage.frame.origin = trans
        
        if gesture.state == .ended{
            
            let velo = gesture.velocity(in: view)
            
            print(velo)
            
            if(velo.y >= 1000 || velo.y <= -1000){
                
                //self.dismiss(animated: true, completion: nil)
                bacShowImage.isHidden = true
                self.bacShowImage.frame.origin = CGPoint(x: 0, y: 65)
            }else{
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.bacShowImage.frame.origin = CGPoint(x: 0, y: 65)
                })
                
            }
        }
        
    }

    
    
    func llamadaWeb(){
        
        
        
        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        let idUser = sessionUser.object(forKey: JSONKey.KEY_ID_USER) as! String
        ref.child(con).child("usuarios").child(idUser).child("imagenes").observe(DataEventType.value, with: { (snapshot) in
            
            
           self.present(self.alert, animated: true){
            if let postDict = snapshot.value as? NSArray{
                
                self.arrayImgs.removeAll()
                self.arrayHeaders.removeAll()
                
                for nam in postDict{
                    let day = (nam as AnyObject).value(forKey: JSONKey.KEY_C_DIA) as! String
                    self.arrayHeaders.append(day)
                    
                    
                    
                    if let arrayIma = (nam as AnyObject).value(forKey: "lstImages") as? NSArray{
                        
                        
                        var arrayArrayImg:[UIImage] = []
                        
                        
                        for img in arrayIma{
                            if((img as? String) != nil){
                            let convertImage = ConvertBase64.DecodingURLPicture(image: "\(JSONKey.URL_IMAGE_BASE)\(img as? String ?? "")")
                            
                            let uiImage = UIImage(data: convertImage as Data)
                            arrayArrayImg.append(uiImage!)
                            }
                        }
                        
                        self.arrayImgs.append(arrayArrayImg)
                    }
                    
                }
                self.cvGalery.reloadData()
                self.alert.dismiss(animated: true, completion: nil)
            
            }else{
                
                
                self.alert.dismiss(animated: true){
                    
                    let message = MDCSnackbarMessage()
                    message.text = "Sin fotos por el momento."
                    MDCSnackbarManager.show(message)
                }
            }
        }
        })
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
            
            if let layout = cvGalery.collectionViewLayout as? UICollectionViewFlowLayout {
                
                print(self.view.frame.size.width )
                
                if(self.view.frame.size.width > 350){
                    let itemWidth = 140
                    let itemHeight = 140
                    layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
                    layout.invalidateLayout()
                }else{
                    
                    let itemWidth = 120
                    let itemHeight = 120
                    layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
                    layout.invalidateLayout()
                    
                }
            }
        
    }
    @IBAction func actionImage(_ sender: UIBarButtonItem) {
        let actionShet =  UIAlertController(title: "Espere un momento", message: "Obteniendo datos.", preferredStyle:.actionSheet)
        actionShet.addAction(UIAlertAction(title: "Traer desde galeria", style: .default, handler: { action in

            print("Galeria")
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
               // let imagePicker = UIImagePickerController()
                self.picker.delegate = self
                self.picker.sourceType = .photoLibrary;
                self.picker.allowsEditing = true
                self.present(self.picker, animated: true, completion: nil)
            }


        }))
        actionShet.addAction(UIAlertAction(title: "Tomar foto", style: .default, handler: { action in

            print("Camara")
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                self.picker.delegate = self
                self.picker.sourceType = .camera;
                self.picker.allowsEditing = true
            
                self.present(self.picker, animated: true, completion: nil)
            }


        }))
        actionShet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))

        self.present(actionShet, animated: true, completion: nil)
    }
    
    @IBAction func actionCloseImg(_ sender: UIButton) {
        
        bacShowImage.isHidden = true
        
    }
    
    //MARK: - Delegates
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            var cropedImage = UIImage()

            if(image.size.width > image.size.height){

                cropedImage = Toucan(image: image).resize(CGSize(width: 900 , height: image.size.height), fitMode: Toucan.Resize.FitMode.crop).image!

            }else{

                cropedImage = Toucan(image: image).resize(CGSize(width: image.size.width , height: 1000), fitMode: Toucan.Resize.FitMode.crop).image!
            }
            
            
            self.dismiss(animated: true){
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "drawableController") as! DrawableViewController
//                vc.img = UIImage()
                vc.img = cropedImage
                self.present(vc, animated: true, completion: nil)
            }
        }else{
            print("nnnnnn")
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
    }
    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    print("jhvhjvghj")
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
    
                print(image)
                if(!arrayHeaders.contains("Mis imagenes")){
                    arrayHeaders.append("Mis imagene")
                }
                arrayImgs[0].append(image)
                cvGalery.reloadData()
        }else{
            
            print("ECHO[00X3]")

        }
        
    }*/
}
extension GaleryViewController: UICollectionViewDataSource, UICollectionViewDelegate{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return arrayImgs[section].count

    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrayHeaders.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellGalery", for: indexPath) as! CustomCollectionViewCell
        
        
        
         cell.imgGalery.image = arrayImgs[indexPath.section][indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let head = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerGalery", for: indexPath) as! CustomCollectionReusableView
        
            head.lblHeadGalery.text = arrayHeaders[indexPath.section]
        
            head.backgroundColor = UIColor.clear
        
        return head
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
        
        let imageName = arrayImgs[indexPath.section][indexPath.item]
        imgShow.contentMode = .scaleToFill
        
        
//        if(imageName.size.width > imageName.size.height){
//            
//            imgShow.frame.size.width = self.view.frame.width - (self.view.frame.width * 0.05)
//            imgShow.frame.size.height = self.view.frame.width - (self.view.frame.width/2)
//            imgShow.center = view.center
//
//        }
//
//        if(imageName.size.height > imageName.size.width){
//
//            imgShow.frame.size.height = self.view.frame.height - (self.view.frame.height * 0.20)
//            imgShow.frame.size.width = self.view.frame.width - (self.view.frame.height * 0.05)
//            imgShow.center = view.center
//            imgShow.frame.origin.y = 15.0
//        }

        
        imgShow.layer.borderColor = UIColor.white.cgColor
        imgShow.layer.borderWidth = CGFloat(Float(1))
        imgShow.image = imageName
        bacShowImage.isHidden = false
    }

}


