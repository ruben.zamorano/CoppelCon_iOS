//
//  PonentsViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 18/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import Alamofire
import UIKit
import Firebase
import MaterialComponents.MaterialSnackbar

class PonentsViewController: UIViewController {

    
    @IBOutlet weak var tvPonents: UITableView!
    var arrayPonents:[PonentObject] = []
//    var alert = UIAlertController()

    /*----------------------------------*/
    var ref: DatabaseReference = Database.database().reference()
    /*-----------------------------------*/
    
    
    let sessionUser = UserDefaults.standard
    
    var alert = UIAlertController(title: "Espere un momento.", message: "Cargando datos", preferredStyle: .alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.alert = UIAlertController(title: "Cargando datos", message: "Por favor espere.", preferredStyle:.alert)
//        self.present(self.alert, animated: true, completion: nil)

        self.present(self.alert, animated: true){
        if !Reachability.isConnectedToNetwork(){

            self.alert.dismiss(animated: true){
            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            }
            
        }
        
        let con = self.sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        
        self.ref.child(con).child("ponentess").observe(DataEventType.value, with: { (snapshot) in
            
            if let postDict = snapshot.value as? NSArray{
          
            self.arrayPonents.removeAll()
            
            for val in postDict{
                
                print("*****************************************")
                print(val)
                print("*****************************************")
                let ponent = PonentObject()
                ponent.codigoVestimenta = (val as AnyObject).value(forKey: JSONKey.KEY_C_CODIGOVESTIMENTA) as? String ?? ""
                ponent.descripcion = (val as AnyObject).value(forKey: JSONKey.KEY_C_DESCRIPCION) as? String ?? ""
                ponent.hora = (val as AnyObject).value(forKey: JSONKey.KEY_C_HORA) as? String ?? ""
                //ponent.id = (val as AnyObject).value(forKey: JSONKey.KEY_ID_PONENTE) as? String ?? ""
                let reqImg = (val as AnyObject).value(forKey: JSONKey.KEY_C_IMAGEN) as? String ?? ""
                
                if(reqImg != ""){
                    if Reachability.isConnectedToNetwork(){
                
                        let convertImage = ConvertBase64.DecodingURLPicture(image: "\(JSONKey.URL_IMAGE_BASE)\(reqImg)")
                        ponent.img = UIImage(data: convertImage as Data)!
                        
                    }else{
                        ponent.img = UIImage(named: "ic_person")!

                    }
                }else{
                    ponent.img = UIImage(named: "ic_person")!

                }
                ponent.mesa = (val as AnyObject).value(forKey: JSONKey.KEY_C_MESA) as? String ?? ""
                ponent.nombre = (val as AnyObject).value(forKey: JSONKey.KEY_C_NOMBREPONENTE) as? String ?? ""
                ponent.tema = (val as AnyObject).value(forKey: "cPuesto") as? String ?? ""
                ponent.ubicacion = (val as AnyObject).value(forKey: JSONKey.KEY_C_UBICACION) as? String ?? ""
                
                
                ponent.puesto = (val as AnyObject).value(forKey: "cPuesto") as? String ?? ""
                self.arrayPonents.append(ponent)
                
                
            }
            
          //  self.alert.dismiss(animated: true, completion: { () -> Void in
            self.tvPonents.reloadData()
                self.alert.dismiss(animated: true, completion: nil)
                
            //})
                
            }else{
                self.alert.dismiss(animated: true){
                let message = MDCSnackbarMessage()
                message.text = "Sin ponentes registrados."
                MDCSnackbarManager.show(message)
                }
                
            }
            
        })
        
        }

        navigationItem.title = "Expositores"

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
  
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if( segue.identifier == "passPonent"){
            
            let cellSelected = tvPonents.indexPathForSelectedRow
            
            
            let ponentView = arrayPonents[(cellSelected?.section)!]
            
            let vc = segue.destination as! DetailsPonentViewController
            vc.ponentDetails.codigoVestimenta = ponentView.codigoVestimenta
            vc.ponentDetails.descripcion = ponentView.descripcion
            vc.ponentDetails.hora = "Hora: \(ponentView.hora)"
            vc.ponentDetails.id = ponentView.id
            vc.ponentDetails.img = ponentView.img
            vc.ponentDetails.tema = ponentView.tema
            vc.ponentDetails.mesa = ponentView.mesa
            vc.ponentDetails.nombre = ponentView.nombre
            vc.ponentDetails.ubicacion = ponentView.ubicacion
            vc.ponentDetails.puesto = ponentView.puesto
            vc.bit = true
            
            
            
        }
    }
}

extension PonentsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayPonents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPonent", for: indexPath) as! CustomTableViewCell
        
        cell.imgPonent.image = arrayPonents[indexPath.section].img
        cell.imgPonent.layer.cornerRadius = cell.imgPonent.frame.width / 2
        cell.imgPonent.clipsToBounds = true
        
        cell.imgPonent.layer.borderColor = UIColor(red: 28/255, green: 136/255, blue: 233/255, alpha: 1.0).cgColor
        cell.imgPonent.layer.borderWidth = CGFloat(Float(4))
        
        cell.lblNamePonent.text = arrayPonents[indexPath.section].nombre
        cell.lblThemePonent.text = arrayPonents[indexPath.section].tema
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 119
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
