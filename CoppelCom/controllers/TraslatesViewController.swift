//
//  TraslatesViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 10/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import MaterialComponents.MaterialSnackbar
class TraslatesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{ //UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var segmentedDay: UISegmentedControl!
    
    @IBOutlet weak var fragmentContainer: UIStackView!
    
    var ref: DatabaseReference = Database.database().reference()

    var arrayDays:[String] = []
    var arrayitems:[[ExitsObject]] = []
    var arrayfragments:[UIView] = []
    var arraysViews:[UIView] = []
    var arrayTableViews:[UITableView] = []
    
    var fragmentactive:UIView?
    let sessionUser = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if !Reachability.isConnectedToNetwork(){
            
            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            
        }
        
        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        
        ref.child(con).child("hoteles").child("traslados").observe(DataEventType.value, with: { (snapshot) in
            
            if let JSON = snapshot.value as? NSArray{
                
                 self.arrayDays.removeAll()
                 self.arrayitems.removeAll()
                 self.arrayfragments.removeAll()
                 self.arraysViews.removeAll()
                self.arrayTableViews.removeAll()
                
                self.fragmentactive?.isHidden = true
                
                var conut = 0
                for days in JSON{
                    
                    print(days)
                    if((days as? NSDictionary) != nil){
                    
                    let day = (days as AnyObject).value(forKey: JSONKey.KEY_C_DIA) as? String ?? ""
                    
                    if let lstTras = (days as AnyObject).value(forKey: JSONKey.KEY_LST_TRASLADOS) as? NSArray{
                        
                        
                        //var arraySalidas:[ExitsObject] = []
                        var arr:[ExitsObject] = []
                        for sal in lstTras{
                         
                            let cSalida = (sal as AnyObject).value(forKey: JSONKey.KEY_C_SALIDA) as? String ?? ""
                            
                           var arrayVehi:[BusesObject] = []
                            
                            
                            if let lstVehi = (sal as AnyObject).value(forKey:"lstVehiculos") as? NSArray{
                                
                                //print("\(lstVehi) ITEMS ---------------------")
                                
                                for vehi in lstVehi{
                                    
                                    let cHora = (vehi as AnyObject).value(forKey: JSONKey.KEY_C_HORA) as? String ?? ""
                                    let cTrans = (vehi as AnyObject).value(forKey: JSONKey.KEY_C_TRANSPORTE) as? String ?? ""
                                 
                                    let vehiculo = BusesObject()
                                    vehiculo.bus = cTrans
                                    vehiculo.hour = cHora
                                    
                                    arrayVehi.append(vehiculo)
                                }
                                
                            }else{
                                
                                let message = MDCSnackbarMessage()
                                message.text = "No hay traslados por el momento."
                                MDCSnackbarManager.show(message)
                            }
                            
                            
                            let salidas = ExitsObject()
                            salidas.salida = cSalida
                            salidas.arrayExits = arrayVehi
                            
                            arr.append(salidas)
                            
                        }
                        
                        self.arrayitems.append(arr)
                        
                    }else{
                        
                        let message = MDCSnackbarMessage()
                        message.text = "No hay traslados por el momento."
                        MDCSnackbarManager.show(message)
                    }
                    

                    self.arrayDays.append(day.uppercased())
                    
                    
                    
                    
                    
                    let fragmenView = UIView(frame: CGRect(x: 0, y: 180, width: self.view.frame.width, height: self.view.frame.height - 180))
                    
                    let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: fragmenView.frame.width, height: fragmenView.frame.height - 33))
                    
                    
                    fragmenView.backgroundColor = UIColor.clear
                    fragmenView.tag = conut
                    if conut != 0{
                    fragmenView.isHidden = true
                    }else{
                    fragmenView.isHidden = false
                        self.fragmentactive = fragmenView
                    }
                    
                    tableView.tag = conut
                  
                    tableView.backgroundColor = UIColor.clear
                    tableView.delegate = self
                    tableView.dataSource = self
                    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell\(conut)")
                    tableView.separatorStyle = .none
                    
                    fragmenView.addSubview(tableView)
                    self.view.addSubview(fragmenView)
                    self.arrayTableViews.append(tableView)
                    self.arrayfragments.append(fragmenView)
                    conut += 1
                }
                }
                
                self.viewDidAppear(true)
                
            }else{
                let message = MDCSnackbarMessage()
                message.text = "No hay traslados por el momento."
                MDCSnackbarManager.show(message)
            }
            
        })

        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        segmentedDay.tintColor = UIColor.clear
        segmentedDay.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black, (NSAttributedStringKey.font as NSCopying) as! AnyHashable:UIFont(name: "Helvetica Neue", size: 15.0)!], for: UIControlState.normal)
        segmentedDay.removeAllSegments()
        
        for v in 0..<arrayDays.count{
            
            let width = self.view.frame.width/CGFloat(self.arrayDays.count)
            let pointView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 4))
            
            //            let txt = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 19))
            //            txt.text = val
            
            pointView.heightAnchor.constraint(equalToConstant: 4).isActive = true
            pointView.widthAnchor.constraint(equalToConstant: width).isActive = true
            
            
            pointView.tag = v
            pointView.backgroundColor = UIColor.white
            self.arraysViews.append(pointView)
            
            pointView.backgroundColor = UIColor.white
            
            if v == 0{
                
                pointView.backgroundColor = UIColor(red: 235/255, green: 107/255, blue: 175/255, alpha: 1)
            }
            
            //fragmentContainer.frame.size.width = self.view.frame.width
            //fragmentContainer.addSubview(pointView)
            self.fragmentContainer.addArrangedSubview(pointView)
            self.fragmentContainer.axis = .horizontal
            self.fragmentContainer.distribution = .equalSpacing
            self.fragmentContainer.alignment = .center
            self.fragmentContainer.spacing = 0.0
            self.fragmentContainer.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            
            segmentedDay.insertSegment(withTitle: arrayDays[v], at: v, animated: true)
        }
        
    }

    @IBAction func actionSegmented(_ sender: UISegmentedControl) {
        
        let selected = arrayfragments[sender.selectedSegmentIndex]
        arraysViews[sender.selectedSegmentIndex].backgroundColor = UIColor(red: 235/255, green: 107/255, blue: 175/255, alpha: 1)
        
        selected.isHidden = false
        
        if(fragmentactive != arrayfragments[sender.selectedSegmentIndex]){
            fragmentactive?.isHidden = true
        }
        fragmentactive = selected
        
        for i in arraysViews{
            
            if(sender.selectedSegmentIndex != i.tag){
                i.backgroundColor = UIColor.white
            }
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell(style: .default, reuseIdentifier: "cell\(tableView.tag)")
        
        if tableView == arrayTableViews[tableView.tag]{
             //tableView.dequeueReusableCell(withIdentifier: "cell\(tableView.tag)", for: indexPath)
            
            cell.selectionStyle = .none
            
            let w = view.frame.width
            let backView = UIView(frame: CGRect(x: 0, y: 0, width: w , height: 19))
            let lblBus = UILabel(frame: CGRect(x: 8, y: 3, width: (w/2)-8, height: 19))
            let lblHor = UILabel(frame: CGRect(x: (w/2), y: 3, width: (w/2)-8, height: 19))
            
            lblBus.font = UIFont(name: "circular", size: 12.0)
            lblHor.font = UIFont(name: "circular", size: 12.0)
            
            lblBus.text = arrayitems[tableView.tag][indexPath.section].arrayExits[indexPath.row].bus
            lblHor.text = arrayitems[tableView.tag][indexPath.section].arrayExits[indexPath.row].hour
            
            
            
            backView.addSubview(lblBus)
            backView.addSubview(lblHor)
            cell.addSubview(backView)
        }
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        print(arrayitems[tableView.tag].count)
        return arrayitems[tableView.tag].count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayitems[tableView.tag][section].arrayExits.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 24
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let back = UIView(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height: 30))
    
        
        back.backgroundColor = UIColor.white//(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
        //headerView.backgroundColor = UIColor.clear
        
        
        let lblSalida = UILabel(frame: CGRect(x: 10, y: 3, width: self.view.frame.width - 40, height: 20))
        lblSalida.numberOfLines = 2
        lblSalida.adjustsFontSizeToFitWidth = true
        let imgBus = UIImageView(frame: CGRect(x: self.view.frame.width - 30, y: 5, width: 24, height: 15))
        imgBus.image = UIImage(named: "ic_bus")
        lblSalida.contentMode = .left
//        lblSalida.heightAnchor.constraint(equalToConstant: 4).isActive = true
        lblSalida.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        
        
        lblSalida.text = "SALIDA: \(arrayitems[tableView.tag][section].salida)"
        lblSalida.font = UIFont(name: "circular-medium", size: 12.0)
        
        
        
        back.addSubview(imgBus)
        back.addSubview(lblSalida)
        
        headerView.addSubview(back)
        return headerView
    }
}
//extension TraslatesViewController: UITableViewDelegate, UITableViewDataSource{
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print(tableView.tag)
//
//        var numberSec = 0
//
//
//
//        return numberSec
//    }
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        var cellBus = CustomTableViewCell()
//
//
//
//
//        return cellBus
//    }
//
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return   19.0
//
//    }
//
//}
//extension TraslatesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        var numberTras = 0
//
//
//
//        return numberTras
//    }
//    //itemDomingo
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell = UICollectionViewCell()
//
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        var size = CGSize()
//
//
//
//        return size
//    }
//}

