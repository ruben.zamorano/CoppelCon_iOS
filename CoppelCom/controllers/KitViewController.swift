//
//  KitViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 11/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import MaterialComponents.MaterialSnackbar

class KitViewController: UIViewController {

    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var tvKit: UITableView!
    
    @IBOutlet weak var segmentesFrg: UISegmentedControl!
    @IBOutlet weak var fragKit: UIView!
    var arrayKit:[String] = []
    var arrayDescKit:[String] = []
    
    @IBOutlet weak var lblDescGift: UILabel!
    var ref: DatabaseReference = Database.database().reference()

    @IBOutlet weak var fragRegalo: UIView!
    
    @IBOutlet weak var frgOne: UIView!
    @IBOutlet weak var frgTwo: UIView!
    
    
    //@IBOutlet weak var imgGift: UIImageView!
    
    
    
    
    
    
    
    let sessionUser = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Crashlytics.sharedInstance().crash()

        navigationItem.title = "Regalo"
        
        viewImage.layer.cornerRadius = viewImage.frame.width / 2
        viewImage.clipsToBounds = true
        
        self.viewImage.layer.masksToBounds = false
        self.viewImage.layer.shadowColor = UIColor.black.cgColor
        self.viewImage.layer.shadowOpacity = 0.5
        self.viewImage.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.viewImage.layer.shadowRadius = 1
        
        
        if !Reachability.isConnectedToNetwork(){
            
            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            
        }
        
        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        
        ref.child(con).child("kit").observe(DataEventType.value, with: { (snapshot) in
            
            
            if let JSON = snapshot.value as? NSArray{
                
                self.arrayKit.removeAll()
                
                for val in JSON{
                print(val)
                    
                    let name = (val as AnyObject).value(forKey:"cNombreKit") as! String
                    let desc = (val as AnyObject).value(forKey:"cDescripcion") as! String
                    self.arrayKit.append(name)
                    self.arrayDescKit.append(desc)
                }
                self.tvKit.reloadData()
            }else{
                
                let message = MDCSnackbarMessage()
                message.text = "Lo sentimos, aun no hay registro de kit. Contacta con staff."
                MDCSnackbarManager.show(message)
                
            }
            
        })
        
        let idUser = sessionUser.object(forKey: JSONKey.KEY_ID_USER) as! String
        ref.child(con).child("usuarios").child(idUser).child("regalo").observe(DataEventType.value, with: { (snapshot) in
            
            
            if let JSON = snapshot.value as? NSDictionary{
                
                let error = JSON.value(forKey: "bError") as! Bool
                
                if(!error){
                    self.lblDescGift.text = JSON.value(forKey: "cMensaje") as? String ?? ""
                    
                }
                
            }
            
        })
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        self.segmentesFrg.tintColor = UIColor.clear
        self.segmentesFrg.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black, (NSAttributedStringKey.font as NSCopying) as! AnyHashable:UIFont(name: "Helvetica Neue", size: 15.0)!], for: UIControlState.normal)
    }
    
    @IBAction func actionSegmented(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            
            frgTwo.backgroundColor = UIColor.white
            frgOne.backgroundColor = UIColor(red: 245/255, green: 75/255, blue: 127/255, alpha: 1.0)
            
            fragKit.isHidden = false
            fragRegalo.isHidden = true
            
            break
        case 1:
            
            frgOne.backgroundColor = UIColor.white
            frgTwo.backgroundColor = UIColor(red: 245/255, green: 75/255, blue: 127/255, alpha: 1.0)
            
            fragKit.isHidden = true
            fragRegalo.isHidden = false
            
            break
        default:
            break
        }
    }
}

extension KitViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayKit.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellKit", for: indexPath) as! CustomTableViewCell
        
        cell.selectionStyle = .none
        cell.lblGiftKit.text = arrayKit[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "\n\n\n\n\n", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let lblDesc = UILabel(frame: CGRect(x: 0, y: 0, width: alert.view.frame.width - 20, height: 120))
        lblDesc.textAlignment = .center
        lblDesc.numberOfLines = 0
        //lblDesc.backgroundColor = UIColor.green
        lblDesc.font = UIFont(name: "", size: 20.0)
        lblDesc.textColor = UIColor.darkGray
        lblDesc.text = arrayDescKit[indexPath.row]
        
        alert.view.addSubview(lblDesc)
        alert.addAction(UIAlertAction(title: "Cerrar", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)

        
    }
}
