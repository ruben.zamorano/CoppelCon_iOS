//
//  DiaryViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 17/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import Crashlytics
import MaterialComponents.MaterialSnackbar

class DiaryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var img_two: UIImageView!
    
    @IBOutlet weak var fragmentContainer: UIStackView!
    
    @IBOutlet weak var segmentDiary: UISegmentedControl!
    
    let tvPrube = UITableView()
    
    let sessionUser = UserDefaults.standard
    
    var arrayDays:[String] = []
    var arraysItems:[[File]] = []
    
    var arraysViews:[UIView] = []
    var arraysViewsBack:[UIView] = []
    var arraysTables:[UITableView] = []
    
    //var alert = UIAlertController(title: "Espere un momento.", message: "Cargando datos", preferredStyle: .alert)
    var activeView:UIView?
    
    var ref: DatabaseReference = Database.database().reference()
    
    var activeDay = 0
    
    let loadBan = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.present(self.alert, animated: true){
            
            self.loadUI()
            
       // }

        

    }
    
    func loadUI(){
        
        if !Reachability.isConnectedToNetwork(){
            
            let message = MDCSnackbarMessage()
            message.text = "Sin conexión a internet."
            MDCSnackbarManager.show(message)
            
        }
        
        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        
        ref.child(con).child("agenda").observe(DataEventType.value, with: { (snapshot) in//.child(idUser).observe(DataEventType.value, with: { (snapshot) in
            
            
            
            if let postDict = snapshot.value as? NSArray{
                
                self.segmentDiary.removeAllSegments()
                self.arrayDays.removeAll()
                self.arraysItems.removeAll()
                
                self.arraysViews.removeAll()
                self.arraysViewsBack.removeAll()
                self.arraysTables.removeAll()
                self.activeView?.isHidden = true
                
                
                
                print(postDict)
                var contWeb = 0
                
                for value in postDict{
                    let day = (value as AnyObject).value(forKey: JSONKey.KEY_C_DIA) as? String ?? ""
                    let lstItinerario = (value as AnyObject).value(forKey: "lstItinerario") as? NSArray ?? []
                    
                    var array:[File] = []
                    for val in lstItinerario{
                        
                        let desc = (val as AnyObject).value(forKey: JSONKey.KEY_C_DESCRIPCION) as? String ?? ""
                        let code = (val as AnyObject).value(forKey: JSONKey.KEY_C_CODIGOVESTIMENTA) as? String ?? ""
                        let hour = (val as AnyObject).value(forKey: JSONKey.KEY_C_HORA) as? String ?? ""
                        let imag = (val as AnyObject).value(forKey: JSONKey.KEY_C_IMAGEN) as? String ?? ""
                        let place = (val as AnyObject).value(forKey: JSONKey.KEY_C_LUGAR) as? String ?? ""
                        let title = (val as AnyObject).value(forKey: JSONKey.KEY_C_TITULO) as? String ?? ""
                        let itine = (val as AnyObject).value(forKey: JSONKey.KEY_N_IDITINERARIO) as? String ?? ""
                        let tipDis = (val as AnyObject).value(forKey: JSONKey.KEY_TIPODEPANTALLA) as? Int ?? -1
                        let numTable = (val as AnyObject).value(forKey: JSONKey.KEY_N_MESA) as? String ?? ""
                        let idPonente = (val as AnyObject).value(forKey: JSONKey.KEY_N_IDPONENTE) as? String ?? "jsdfjk"
                        
                        print("Comineza-------------------------")
                        print(idPonente)
                        
                        //let convertImage = ConvertBase64.DecodingURLPicture(image: "\(JSONKey.URL_IMAGE_BASE)\(imag)")
                        
                        let itineObj = File()
                        itineObj.codigo = code
                        itineObj.descripcion = desc
                        itineObj.hora = hour
                        itineObj.id = itine
                        itineObj.idPonente = idPonente
                        itineObj.img = imag
                        itineObj.num_mesa = numTable
                        itineObj.tipoPantalla = tipDis
                        itineObj.titulo = title
                        itineObj.lugar = place
                        
                        if let lstPnt = (val as AnyObject).value(forKey: "lstIdPonentes") as? NSArray {
                            print(lstPnt)
                            
                            itineObj.lstSubPonents = lstPnt as! [String]
                        }
                        
                        array.append(itineObj)
                        
                    }
                    
                    self.arraysItems.append(array)
                    self.arrayDays.append(day.uppercased())
                    
                    
                    
                    var tableContainer = UIView()
                    
                    print(self.view.frame.height)
                    
                    if(self.view.frame.height < 668){
                    tableContainer = UIView(frame: CGRect(x: 0, y: 104, width: self.view.frame.width, height: self.view.frame.height-136))
                    }else{

                         tableContainer = UIView(frame: CGRect(x: 0, y: 124, width: self.view.frame.width, height: self.view.frame.height-136))
                    }
                
                    
                    tableContainer.backgroundColor = UIColor.clear
                    let table = UITableView(frame: CGRect(x: 0, y: 0, width: tableContainer.frame.width, height: tableContainer.frame.height))
                    //table.center = tableContainer.center
                    
                    tableContainer.isHidden = true
                    
                    tableContainer.tag = contWeb
                    self.arraysViewsBack.append(tableContainer)
                    
                    
                    
                    self.view.addSubview(tableContainer)
                    
                    if(contWeb == 0){
                        tableContainer.isHidden = false
                        self.activeView = tableContainer
                    }
                    
                    table.register(UITableViewCell.self, forCellReuseIdentifier: "cell\(contWeb)")
                    table.tag = contWeb
                    
                    //fragmentContainer.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
                    
                    table.delegate = self
                    table.dataSource = self
                    table.backgroundColor = UIColor.clear
                    self.arraysTables.append(table)
                    
                    tableContainer.addSubview(table)
                    contWeb += 1
                }
                
                self.loadMenu()
              //  self.alert.dismiss(animated: true, completion: nil)
                
            }else{
              //  self.alert.dismiss(animated: true, completion: nil)
                
                let message = MDCSnackbarMessage()
                message.text = "Aún no hay eventos agendados."
                MDCSnackbarManager.show(message)
                
            }
            
        })
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
     
        
    }
    
    func loadMenu(){
        
        self.segmentDiary.removeAllSegments()
        var cont = 0
        for val in self.arrayDays{
            
            
            let width = self.view.frame.width/CGFloat(self.arrayDays.count)
            let pointView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 4))
            
            //            let txt = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 19))
            //            txt.text = val
            
            pointView.heightAnchor.constraint(equalToConstant: 4).isActive = true
            pointView.widthAnchor.constraint(equalToConstant: width).isActive = true
            
            
            pointView.tag = cont
            pointView.backgroundColor = UIColor.white
            self.arraysViews.append(pointView)
            
            
            
            if cont == 0{
                
                
                pointView.backgroundColor = UIColor(red: 235/255, green: 107/255, blue: 175/255, alpha: 1)
            }
            
            //fragmentContainer.frame.size.width = self.view.frame.width
            //fragmentContainer.addSubview(pointView)
            self.fragmentContainer.addArrangedSubview(pointView)
            self.fragmentContainer.axis = .horizontal
            self.fragmentContainer.distribution = .equalSpacing
            self.fragmentContainer.alignment = .center
            self.fragmentContainer.spacing = 0.0
            self.fragmentContainer.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            //fragmentContainer.translatesAutoresizingMaskIntoConstraints = false;
            //fragmentContainer.centerYAnchor.constraint(equalTo: 36).isActive = true
            
            
            
            self.segmentDiary.insertSegment(withTitle: val, at: cont, animated: true)
            
            
            cont += 1
        }
        
        
        self.segmentDiary.tintColor = UIColor.clear
        self.segmentDiary.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black, (NSAttributedStringKey.font as NSCopying) as! AnyHashable:UIFont(name: "Helvetica Neue" , size: 15.0)!], for: UIControlState.normal)
        
    }

    
    @IBAction func segmentedAction(_ sender: UISegmentedControl) {
       
        
        activeDay = sender.selectedSegmentIndex
       arraysViews[sender.selectedSegmentIndex].backgroundColor = UIColor(red: 235/255, green: 107/255, blue: 175/255, alpha: 1)
        
        
        arraysViewsBack[sender.selectedSegmentIndex].isHidden = false
        
        if(activeView != arraysViewsBack[sender.selectedSegmentIndex]){
            activeView?.isHidden = true
        }
        
        for i in arraysViews{
            
            if(sender.selectedSegmentIndex != i.tag){
                i.backgroundColor = UIColor.white
            }
            
        }
        activeView = arraysViewsBack[sender.selectedSegmentIndex]
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return arraysItems[tableView.tag].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cellRet = UITableViewCell(style: .subtitle, reuseIdentifier: "cell\(tableView.tag)")
        
        if tableView.tag == arraysTables[tableView.tag].tag{
            
            //cellRet = tableView.dequeueReusableCell(withIdentifier: "cell\(tableView.tag)", for: indexPath) 

            //let myAttr = [NSAttributedStringKey.font:UIFont(name: "Helvetica Neue", size: 24.0) ]
        //cellRet = UITableViewCell(style: .subtitle, reuseIdentifier: "cell\(tableView.tag)")
            
            print(" dnfgjkndkjfgnjkdsfngjk \(arraysItems[tableView.tag][indexPath.section].descripcion)")
            
            let html =  arraysItems[tableView.tag][indexPath.section].descripcion
            let data = html.data(using: .utf8)!
            let att = try! NSAttributedString.init(
                data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            let matt = NSMutableAttributedString(attributedString:att)
            matt.enumerateAttribute(
                NSAttributedStringKey.font,
                in:NSMakeRange(0,matt.length),
                options:.longestEffectiveRangeNotRequired) { value, range, stop in
                    let f1 = value as! UIFont
                    let f2 = UIFont(name:"circular", size:14)!
                    
                    if let f3 = applyTraitsFromFont(f1, to:f2) {
                        matt.addAttribute(
                            NSAttributedStringKey.font, value:f3, range:range)
                    }
            }
            
            let rep = "\(matt)".replacingOccurrences(of: "{", with: "")
            cellRet.detailTextLabel?.text = rep
            
            //cellRet.detailTextLabel?.font = UIFont(name: "Helvetica Neue", size: 19.0)
            
            cellRet.textLabel?.font = UIFont(name: "circular", size: 20.0)
            cellRet.textLabel?.text = arraysItems[tableView.tag][indexPath.section].titulo
            cellRet.accessoryType = .disclosureIndicator
            cellRet.selectionStyle = .none
            //cellRet.imageView?.image = UIImage(named:"ic_person")
            cellRet.detailTextLabel?.textColor = UIColor.lightGray
            cellRet.detailTextLabel?.font = UIFont(name: "circular", size: 15.0)
            
            
        }
        
        return cellRet
        
        
    }
    func applyTraitsFromFont(_ f1: UIFont, to f2: UIFont) -> UIFont? {
        let t = f1.fontDescriptor.symbolicTraits
        if let fd = f2.fontDescriptor.withSymbolicTraits(t) {
            return UIFont.init(descriptor: fd, size: 0)
        }
        return nil
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let back = UIView(frame: CGRect(x: 0, y: 15, width: self.view.frame.width, height: 30))
        
        
        
        
        back.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
        //headerView.backgroundColor = UIColor.clear
        
        
        let lblHour = UILabel(frame: CGRect(x: 38, y: 3, width: self.view.frame.height, height: 20))
        lblHour.text = "Hora: \(arraysItems[tableView.tag][section].hora)"
        lblHour.textColor = UIColor.darkGray
        //lblHour.font = UIFont(name: "didonesque", size: 15.0)
        
        let imgClock = UIImageView(frame: CGRect(x: 10, y: 0, width: 25, height: 25))
        imgClock.image = UIImage(named:"ic_clock")
        back.addSubview(lblHour)
        back.addSubview(imgClock)
        headerView.addSubview(back)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  75
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tipo = arraysItems[tableView.tag][indexPath.section].tipoPantalla
        
//        print(arraysItems[tableView.tag][indexPath.section].titulo)
//        print(arraysItems[tableView.tag][indexPath.section].tipoPantalla)
        
        
        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        let idUser = sessionUser.object(forKey: JSONKey.KEY_ID_USER) as! String
        print("ID USER: \(idUser)")
        print("ID USER: \(indexPath.section)")
        ref.child(con).child("agendasusuarios").child(idUser).child("\(activeDay)").child("lstItinerario").child("\(indexPath.section)").observe(DataEventType.value, with: { (snapshot) in
            
            
            print(snapshot)
            if let JSON = snapshot.value as? NSDictionary{
                
                
                print(JSON)
                self.goto(tipo: tipo, mesa: JSON.value(forKey: "nMesa") as! String, obj: self.arraysItems[tableView.tag][indexPath.section])
                
            }else{
                
                print("fuck")
            }
            
        })
        
        print("TIPO DE PANTALLA \(tipo)")
        
    }
    
    func goto(tipo:Int, mesa:String, obj:File){
        if(tipo == 2){
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailsConference") as! DetailsEventViewController
            vc.present = obj
            vc.GETmesa = mesa//JSON.value(forKey: "cMesa") as! String
            self.navigationController?.pushViewController(vc,animated: true)
            
            
        }else{
            
            
            print("TIPO UNO")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailsEvent") as! DetailsEventsViewController
            vc.event = obj
            vc.GETmesa = mesa //JSON.value(forKey: "cMesa") as! String
            self.navigationController?.pushViewController(vc,animated: true)
            
        }
    }
    
}

