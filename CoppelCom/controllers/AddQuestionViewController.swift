//
//  AddQuestionViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 10/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MaterialSnackbar

class AddQuestionViewController: UIViewController {

    @IBOutlet weak var cardViewComent: UIView!
    @IBOutlet weak var btnSentQuestion: UIButton!
    
    
    @IBOutlet weak var txtNameUser: UITextField!
    @IBOutlet weak var txtQuestion: UITextView!
    
    
    var textFocusesd = UITextField()
    var textViewFocusesd = UITextView()
    
    var arrayQuestion:[QuestionsObjects] = []
    
    var ref: DatabaseReference = Database.database().reference()
let sessionUser = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.someAction))
        view.addGestureRecognizer(gesture)
        
        let name = sessionUser.object(forKey: JSONKey.KEY_C_NOMBRE_EMPLEADO) as! String
        
        print("----------\(name)")
        txtNameUser.text = name
        
        if !Reachability.isConnectedToNetwork(){
            
            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            
        }
        
        ref.child("preguntas").observe(DataEventType.value, with: { (snapshot) in
            
            print(snapshot.value!)
            
            if let postDict = snapshot.value as? NSArray{
        
            self.arrayQuestion.removeAll()
            
            
                for val in postDict{
                
                
                
                let nombre = (val as AnyObject).value(forKey: "cNombres") as? String ?? "Anonimo"
                let pregunta = (val as AnyObject).value(forKey: "cPregunta") as? String ?? ""
                let respuesta = (val as AnyObject).value(forKey: "cRespuesta") as? String ?? ""
                let idUser = (val as AnyObject).value(forKey: "nIdUsuario") as? Int ?? 0
            
                let pre = QuestionsObjects()
                pre.nombreUsuario = nombre
                pre.question = pregunta
                pre.response = respuesta
                pre.idUsuario = idUser
                
                self.arrayQuestion.append(pre)
            
            }
            }
        })
        
        txtQuestion.textColor = UIColor.lightGray

        self.navigationItem.title = "Preguntas"
        self.navigationItem.backBarButtonItem?.title = "Atras"
        
        self.cardViewComent.layer.masksToBounds = false
        self.cardViewComent.layer.shadowColor = UIColor.lightGray.cgColor
        self.cardViewComent.layer.shadowOpacity = 0.3
        self.cardViewComent.layer.shadowOffset = CGSize(width: -0.5, height: 1)
        self.cardViewComent.layer.shadowRadius = 3
        
        
        btnSentQuestion.layer.cornerRadius = btnSentQuestion.frame.width / 2
        btnSentQuestion.clipsToBounds = true
        
        
        
      
    }
  
    @IBAction func addQuestion(_ sender: UIButton) {
        
        sender.isEnabled = false
        var name = txtNameUser.text!
        let question = txtQuestion.text!
        let idUser = sessionUser.object(forKey: JSONKey.KEY_ID_USER) as! String
        
        if(name == ""){
            
            name = "Anonimo"
        }
        
        if(question != "Escribe tu pregunta"){
            

            self.ref.child("preguntas/\(self.arrayQuestion.count)").setValue(["cNombres":name, "cPregunta":question, "cRespuesta":"", "nIdUsuario":idUser])

            
            txtQuestion.text = "Escribe tu pregunta"
            txtQuestion.resignFirstResponder()
            
            let message = MDCSnackbarMessage()
            message.text = "Pregunta enviada."
            MDCSnackbarManager.show(message)
            
        }else{
            
            let alert = UIAlertController(title: "Espera!", message: "Campos vacios, debes agregar un pregunta!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    @objc func someAction(){
        
        txtQuestion.resignFirstResponder()
    }
}

extension AddQuestionViewController: UITextFieldDelegate, UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
         textViewFocusesd = textView
        if(textView.text! == "Escribe tu pregunta"){
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
         textFocusesd = textField
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        if(textView.text! == ""){
            
            textView.text = "Escribe tu pregunta"
            textView.textColor = UIColor.lightGray
        }
        
        return true
    }
}
