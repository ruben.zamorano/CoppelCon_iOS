//
//  QRCodeViewController.swift
//  CoppelCom
//
//  Created by MacBook Pro on 28/12/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import UIKit

class QRCodeViewController: UIViewController {

    
    @IBOutlet weak var lblCode: UILabel!
    
    let sessionUser = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        let code = sessionUser.object(forKey: JSONKey.KEY_CODE_FOTO) as? String ?? "Sin codigo"
        lblCode.text = code
        
        
    }

}
