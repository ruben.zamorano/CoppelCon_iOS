//
//  DetailsPonentViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 18/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Firebase
//import FirebaseAuth

class DetailsPonentViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var imgPonent: UIImageView!
    @IBOutlet weak var lblNombrePonent: UILabel!
    @IBOutlet weak var lblTthemePonent: UILabel!
    @IBOutlet weak var lblDescriptionPonent: UILabel!
    @IBOutlet weak var lblPlacePonent: UILabel!
    @IBOutlet weak var lblTablePonent: UILabel!
    @IBOutlet weak var clickQuestion: UIView!
    @IBOutlet weak var lblHourPonent: UILabel!
    @IBOutlet weak var lblCodeDress: UILabel!
    @IBOutlet weak var viewData: UIView!
    
    var alert = UIAlertController()
    
    var ponentDetails = PonentObject()
    
    var bit = false
    var t = Timer()
    
    
    /*----------------------------------*/
    //var ref: DatabaseReference = Database.database().reference()
    /*-----------------------------------*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("----------------------> \(ponentDetails.ubicacion)")
        print("----------------------> \(ponentDetails.hora)")
        print("----------------------> \(ponentDetails.mesa)")
        print("----------------------> \(ponentDetails.codigoVestimenta)")
        
        
       inicializaciones()
        
        lblCodeDress.text = ""
      
        alert = UIAlertController(title: "Espere un momento.", message: "Cargando datos", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        lblCodeDress.text = "Codigo de vestimenta: \(ponentDetails.codigoVestimenta)"
        lblHourPonent.text = "Hora: \(ponentDetails.hora)"
        lblPlacePonent.text = "Lugar: \(ponentDetails.ubicacion)"
        lblTablePonent.text = "# \(ponentDetails.mesa)"
        lblNombrePonent.text = ponentDetails.nombre
        lblTthemePonent.text = ponentDetails.puesto
        
        
        let html = ponentDetails.descripcion
        let data = html.data(using: .utf8)!
        let att = try! NSAttributedString.init(
            data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        let matt = NSMutableAttributedString(attributedString:att)
        matt.enumerateAttribute(
            NSAttributedStringKey.font,
            in:NSMakeRange(0,matt.length),
            options:.longestEffectiveRangeNotRequired) { value, range, stop in
                let f1 = value as! UIFont
                let f2 = UIFont(name:"Helvetica Neue", size:17)!
                if let f3 = applyTraitsFromFont(f1, to:f2) {
                    matt.addAttribute(
                        NSAttributedStringKey.font, value:f3, range:range)
                }
        }
        
        
        lblDescriptionPonent.attributedText = matt//ponentDetails.descripcion.convertHtml()
        lblDescriptionPonent.sizeToFit()
        imgPonent.image = ponentDetails.img
        
        
        viewData.isHidden = true
            
        
        
    }
    func applyTraitsFromFont(_ f1: UIFont, to f2: UIFont) -> UIFont? {
        let t = f1.fontDescriptor.symbolicTraits
        if let fd = f2.fontDescriptor.withSymbolicTraits(t) {
            return UIFont.init(descriptor: fd, size: 0)
        }
        return nil
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
      Timer.scheduledTimer(timeInterval: 1 , target: self, selector: #selector(self.update), userInfo: nil, repeats: false);
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.someAction))
        clickQuestion.addGestureRecognizer(gesture)
    }
    
    @objc func update() {
        
       
      
        let suma = 280 + lblDescriptionPonent.frame.height + lblTablePonent.frame.height + lblHourPonent.frame.height + lblPlacePonent.frame.height + lblCodeDress.frame.height // + clickQuestion.frame.height
        
        
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: suma + 244)
        scrollView.layoutIfNeeded()
        scrollView.clipsToBounds = true

        self.alert.dismiss(animated: true, completion: nil)
        
        
    }

    func inicializaciones(){
     
        scrollView.delegate = self
        
        navigationItem.title = "Detalle de ponente"
        self.navigationItem.backBarButtonItem?.title = "Atras"
        
        self.clickQuestion.layer.masksToBounds = false
        self.clickQuestion.layer.shadowColor = UIColor.black.cgColor
        self.clickQuestion.layer.shadowOpacity = 0.5
        self.clickQuestion.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.clickQuestion.layer.shadowRadius = 1
        
    }
    
    @objc func someAction() {
        
        print("ACTION")
        //let suma = 280 + lblDescriptionPonent.frame.height + lbl
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "addQuestionController") as! AddQuestionViewController
        //vc.newsObj = newsObj
        navigationController?.pushViewController(vc,animated: true)
        
    }

}

