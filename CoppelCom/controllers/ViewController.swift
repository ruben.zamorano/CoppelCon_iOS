//
//  ViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 07/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import FirebaseMessaging
import Alamofire
import UserNotifications
import MaterialComponents.MaterialSnackbar

class ViewController: UIViewController, UITextFieldDelegate {

    let sessionUser = UserDefaults.standard
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    var textFocused = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: {didAlow, err in})
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.someAction))
        view.addGestureRecognizer(gesture)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         
    }

    @IBAction func actionLogin(_ sender: UIButton) {
        
        let token = Messaging.messaging().fcmToken
        print("FCM token Send: \(token ?? "")")
        let params:Parameters = ["cMail":txtEmail.text!,"cPassword":txtPass.text!, "fcm_token":token ?? ""]
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]



        Alamofire.request(JSONKey.URL_LOGIN, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON{
            res in

            if let JSON = res.result.value as? NSDictionary{

                print(JSON)
                let respuesta = JSON.value(forKey: "bError") as! Bool
                let msg = JSON.value(forKey: "cMensaje") as! String

                if(!respuesta){

                    self.sessionUser.set( JSON.value(forKey: "nIdEmpleado") as! String , forKey: JSONKey.KEY_ID_USER)
                    //self.sessionUser.synchronize()
        
                    let idNacionl = JSON.value(forKey: "nIdConvencionNacional") as? String ?? ""
                    let idRegional = JSON.value(forKey: "nIdConvencionRegional") as? String ?? ""
                    let nombreEmp = JSON.value(forKey: "cNombres") as? String ?? ""
                    let codeFoto = JSON.value(forKey: "cCodigoFoto") as? String ?? ""
                    let nombrePar = JSON.value(forKey: "cNombrePareja") as? String ?? "Sin Pareja"
                    let email = JSON.value(forKey: "cMail") as? String ?? ""
                    print("NOMBRE EMPLEADO----->\(nombreEmp)")
                    
//            self.sessionUser.set( "5a2c268c7abb8d0000000001" , forKey: JSONKey.KEY_ID_USER)
//            self.sessionUser.synchronize()
        
                    if(idNacionl != "" || idRegional != ""){
                        
                        if(idNacionl != ""){
                            print("ID NACIONAL----->\(idNacionl)")
                        self.sessionUser.set(idNacionl, forKey: JSONKey.KEY_CON_NAC)
                        }
                        if(idRegional != ""){
                            print("ID REGIONAL----->\(idRegional)")
                            self.sessionUser.set(idRegional, forKey: JSONKey.KEY_CON_REG)
                        }
                        
                        self.sessionUser.set(nombreEmp, forKey: JSONKey.KEY_C_NOMBRE_EMPLEADO)
                        self.sessionUser.set(nombrePar, forKey: JSONKey.KEY_B_PAREJA)
                        self.sessionUser.set(email, forKey: JSONKey.KEY_EMAIL)
                        self.sessionUser.set(codeFoto, forKey: JSONKey.KEY_CODE_FOTO)
                        self.sessionUser.synchronize()
                        
                        
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "convencionController") as! UINavigationController
                        
                        self.present(vc, animated: true, completion: nil)
                        
                        
                    }else{
                        
                        let message = MDCSnackbarMessage()
                        message.text = "Lo sentimos usted no cuenta con ninguna convención asignada"
                        MDCSnackbarManager.show(message)
                        
                    }
                }else{

                    let message = MDCSnackbarMessage()
                    message.text = msg
                    MDCSnackbarManager.show(message)

                }

            }else{

                let message = MDCSnackbarMessage()
                message.text = "Sin conexión con el servidor."
                MDCSnackbarManager.show(message)
                print("Request: \(res.request)")
                print("Error: \(res.error)")

            }

        }
        
    }
    
  
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textFocused = textField
    }
    @objc func someAction(){
        
        textFocused.resignFirstResponder()
    }

}
