//
//  HomeViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 07/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Crashlytics
import ZDCChat
import Firebase
import MaterialComponents.MaterialSnackbar


class HomeViewController: UIViewController, UIScrollViewDelegate, UIPopoverPresentationControllerDelegate, backDelegate, logoutDelegate {

    
    @IBOutlet weak var segmentedView: UISegmentedControl!
    @IBOutlet weak var pagecontroll: UIPageControl!
    
    @IBOutlet weak var backViewOne: UIView!
    @IBOutlet weak var backViewTwo: UIView!
    
    @IBOutlet weak var noBanner: UIView!
    
    @IBOutlet weak var fragmentOne: UIView!
    @IBOutlet weak var fragmentTwo: UIView!
    @IBOutlet weak var stackFragments: UIStackView!
    
    @IBOutlet weak var scrollView: UIScrollView!
  
    var timer = Timer()
    
    
    var ref: DatabaseReference = Database.database().reference()
    
    var arrayColors:[UIColor] = [UIColor.black, UIColor.blue, UIColor.cyan, UIColor.green]
    var arrayCarousel:[UIImage] = []

    
    var arraItems:[String] = ["agenda", "ponentes", "preguntas", "regalo", "fotos", "kit"]
    var arraItemsTwo:[String] = ["transporte", "hotel", "traslado"]
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var alert = UIAlertController(title: "Espere un momento.", message: "Cargando datos", preferredStyle: .alert)
    
    

    let sessionUser = UserDefaults.standard
    
    @IBAction func actionChat(_ sender: Any) {
        
        ZDCChat.start(in: self.navigationController, withConfig: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        print("ID CONVENCIOOON ---------------")
        print(sessionUser.object(forKey: JSONKey.KEY_CON) as! String)
        
        self.noBanner.isHidden = true
        self.pagecontroll.isHidden = false
        
        self.present(self.alert, animated: true){
        if !Reachability.isConnectedToNetwork(){
            
            self.alert.dismiss(animated: true){
                self.noBanner.isHidden = false
                self.pagecontroll.isHidden = true
            let message = MDCSnackbarMessage()
            message.text = "Sin conexión a internet."
            MDCSnackbarManager.show(message)
            }
            
        }else{
        
        let con = self.sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        
        self.ref.child(con).child("banner").observe(DataEventType.value, with: { (snapshot) in
            
            
            if let JSON = snapshot.value as? NSArray{
            
                self.arrayCarousel.removeAll()
                //self.segmentedView.removeAllSegments()
                
                for img in JSON{
                    print("ftyvfytfytftftftftftfyfytfytfytftftftftyftfty")
                    let cnv = ConvertBase64.DecodingURLPicture(image: "\(JSONKey.URL_IMAGE_BASE)\(img as! String)")
                    self.arrayCarousel.append(UIImage(data: cnv as Data)!)
                }
                
                
                    self.initUI()
                    
                
                
            }else{
                self.alert.dismiss(animated: true){
                    
                    self.noBanner.isHidden = false
                    self.pagecontroll.isHidden = true
//                let message = MDCSnackbarMessage()
//                message.text = "Error al cargar imagenes."
//                MDCSnackbarManager.show(message)
                }
            }
            
        })
        }
    }
        
        if(sessionUser.object(forKey: JSONKey.KEY_CON_NAC) != nil){
            
            navigationItem.title = "Convención SLP 18"
            
        }else{
            
            navigationItem.title = "Convención RCUL"
        }
     
        timer = Timer.scheduledTimer(timeInterval: 5, target: self,   selector: (#selector(HomeViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @IBAction func actionShowNotifications(_ sender: UIBarButtonItem) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "notificationController") as! NotificationViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    func initUI(){
        
        scrollView.frame.size.height = 200
        
        scrollView.contentSize = CGSize(width: (self.view.frame.size.width * CGFloat(arrayCarousel.count)), height: self.scrollView.frame.size.height)
        
        
        
        
        scrollView.delegate = self
        
        
        pagecontroll.numberOfPages = arrayCarousel.count
        for index in 0..<arrayCarousel.count{
            
            frame.origin.x = (self.view.frame.size.width /*- 34*/) * CGFloat(index)
            frame.size = self.view.frame.size
            
            let v = UIView(frame: frame)
            let imgView = UIImageView()
            //v.backgroundColor = arrayImgs[index]
            //v.frame.size.width = self.view.framee.size.width
            //v.center = CGPoint(x: self.view.frame.size.width * CGFloat(index) + 130, y: 100)
            
            imgView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.scrollView.frame.size.height)
            
            imgView.contentMode = .scaleToFill
            
            
            imgView.image = arrayCarousel[index]
            
            v.addSubview(imgView)
            
            self.scrollView.addSubview(v)
        }
        self.alert.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    //    Crashlytics.sharedInstance().crash()
    
       
        segmentedView.tintColor = UIColor.clear
        segmentedView.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white, (NSAttributedStringKey.font as NSCopying) as! AnyHashable:UIFont(name: "Helvetica Neue", size: 17.0)!], for: UIControlState.normal)
        
        
        
        
        
        
    }
   
    @objc func updateTimer(){
       
        let pageNumber = scrollView.contentOffset.x / self.view.frame.size.width
        
        if(Int(pageNumber + 1) != arrayCarousel.count){
            
            let pageNumber = scrollView.contentOffset.x / self.view.frame.size.width
            pagecontroll.currentPage = Int(pageNumber + 1)
            //pagecontroll.currentPageIndicatorTintColor = UIColor.white
            
            let x = CGFloat(pagecontroll.currentPage) * self.self.view.frame.size.width
            let point = CGPoint(x: x , y:0)
            scrollView.setContentOffset(point, animated: true)
            
            
        }else{
            
            scrollView.contentOffset = CGPoint(x: 0 , y: 0)
            
            pagecontroll.currentPage = 0
            //pagecontroll.currentPageIndicatorTintColor = UIColor.white
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / self.view.frame.size.width
        pagecontroll.currentPage = Int(pageNumber + 1)
        //pagecontroll.currentPageIndicatorTintColor = UIColor.white //(red: 244, green: 108, blue: 125, alpha: 1.0)
    }
    
    
    @IBAction func actionSection(_ sender: UISegmentedControl) {
        print("simon")
        if (sender.selectedSegmentIndex == 0) {

            fragmentOne.backgroundColor = UIColor(red: 51/255, green: 217/255, blue: 194/255, alpha: 1.0)
            fragmentTwo.backgroundColor = UIColor(red: 28/255, green: 136/255, blue: 233/255, alpha: 1.0)
            backViewOne.isHidden = false
            backViewTwo.isHidden = true

        }else if(sender.selectedSegmentIndex == 1){
            
            fragmentTwo.backgroundColor = UIColor(red: 51/255, green: 217/255, blue: 194/255, alpha: 1.0)
            fragmentOne.backgroundColor = UIColor(red: 28/255, green: 136/255, blue: 233/255, alpha: 1.0)
            backViewOne.isHidden = true
            backViewTwo.isHidden = false

        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "popOverSegue"){
            
            segue.destination.popoverPresentationController?.delegate = self
            let sendContext = segue.destination as! popOverViewController
            sendContext.delegateBack = self
            sendContext.salir = self
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @IBAction func showProfile(_ sender: UIBarButtonItem) {
        
        
    }
    
    @IBAction func actionMenu(_ sender: UIButton) {
        
    }
    
    @IBAction func actionPageControll(_ sender: UIPageControl) {
        
        let x = CGFloat(sender.currentPage) * self.view.frame.size.width //+ 110
        
        scrollView.contentOffset = CGPoint(x: x, y: 0)
    }
    
    func anotherCon() {
        print("back2")
        sessionUser.removeObject(forKey: JSONKey.KEY_CON)
//        sessionUser.removeObject(forKey: JSONKey.KEY_CON_NAC)
//        sessionUser.removeObject(forKey: JSONKey.KEY_CON_REG)
        sessionUser.synchronize()
        navigationController?.popViewController(animated: true)
    }
    
    func logOut() {
        sessionUser.removeObject(forKey: JSONKey.KEY_ID_USER)
        sessionUser.removeObject(forKey: JSONKey.KEY_CON)
        sessionUser.removeObject(forKey: JSONKey.KEY_CON_REG)
        sessionUser.removeObject(forKey: JSONKey.KEY_CON_NAC)
        sessionUser.synchronize()
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "logInController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
}

