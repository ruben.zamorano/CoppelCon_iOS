//
//  QuickViewController.swift
//  CoppelCom
//
//  Created by MacBook Pro on 08/12/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MaterialSnackbar
import Alamofire


class QuickViewController: UIViewController {

    
    let sessionUser = UserDefaults.standard
    @IBOutlet weak var tvResponse: UITableView!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var questionShadow: UIView!
    
    var ref: DatabaseReference = Database.database().reference()
    
//    var _bandera = true
    var _id = ""
    var cNombreEncuesta = ""
    var arrayQuestions:[QuickObject] = []
    
    var arrayQuestionsSend:[String] = []
    @IBOutlet weak var viewCard: UIView!
    
    struct Beer {
        var nValor: Int
        var cRespuesta: String
        var bSeleccionada: Bool
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        self.viewCard.layer.masksToBounds = false
        self.viewCard.layer.shadowColor = UIColor.lightGray.cgColor
        self.viewCard.layer.shadowOpacity = 0.3
        self.viewCard.layer.shadowOffset = CGSize(width: -0.5, height: 1)
        self.viewCard.layer.shadowRadius = 4
        
        self.questionShadow.layer.masksToBounds = false
        self.questionShadow.layer.shadowColor = UIColor.lightGray.cgColor
        self.questionShadow.layer.shadowOpacity = 0.3
        self.questionShadow.layer.shadowOffset = CGSize(width: -0.5, height: 1)
        self.questionShadow.layer.shadowRadius = 4
        
//        viewCard.layer.borderColor = UIColor(red: 28/255, green: 136/255, blue: 233/255, alpha: 1.0).cgColor
//        viewCard.layer.borderWidth = CGFloat(Float(4.0))
        
        
        if !Reachability.isConnectedToNetwork(){
            
            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            
        }else{
                //if(_bandera){
                    
                let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
                print(con)
                ref.child(con).child("encuesta").observe(DataEventType.value, with: { (snapshot) in
                    
                    //self._bandera = true
                    if let postDict = snapshot.value as? NSDictionary{
                        
                        self.arrayQuestions.removeAll()

                        self._id = postDict.value(forKey: "_id") as? String ?? "Sin encuesta"
                        self.cNombreEncuesta = postDict.value(forKey: "cNombreEncuesta") as? String ?? "Por el momento no hay encuestas disponibles"
                        if let encuesta = postDict.value(forKey: "lstRespuestas") as? NSArray{
                        print(encuesta)
                        for items in encuesta{

                            let res = QuickObject()
                            res.bSeleccionada = (items as AnyObject).value(forKey: "bSeleccionada") as! Bool
                            res.cRespuesta = (items as AnyObject).value(forKey: "cRespuesta") as! String
                            res.nValor = (items as AnyObject).value(forKey: "nValor") as! Int

                            self.arrayQuestions.append(res)
                        }
                    }
                        
                        
                        self.lblQuestion.text = self.cNombreEncuesta
                        self.tvResponse.reloadData()


                    }
                })
                
                
//                }else{
//
//                    self.lblQuestion.text = "No hay encuesta por el momento"
//            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
       
        
    }

    @IBAction func actionSend(_ sender: UIButton) {
        
        let index = tvResponse.indexPathForSelectedRow
        
        //print(arrayQuestions[(index?.row)!].cRespuesta)
        
        
        if index != nil {
        for it in arrayQuestions{
            
            let obj = QuickObject()
            
            var objSend = ""
            obj.cRespuesta = it.cRespuesta
            obj.nValor = it.nValor
            
            if(it.nValor != arrayQuestions[(index?.row)!].nValor)
            {
                
            
                objSend =   "{\"cRespuesta\":\"\(it.cRespuesta)\",\"nValor\":\(it.nValor),\"bSeleccionada\":\(false)}"
                obj.bSeleccionada = false
                //s = ["cRespuesta":it.cRespuesta,"bSeleccionada":false,"nValor":it.nValor]
                
            }else{

                objSend = "{\"cRespuesta\":\"\(it.cRespuesta)\",\"nValor\":\(it.nValor),\"bSeleccionada\":\(true)}"
                //obj.bSeleccionada = true
                //s = ["cRespuesta":it.cRespuesta,"bSeleccionada":true,"nValor":it.nValor]
                
            }
            
            
            
            arrayQuestionsSend.append(objSend)
        }
        
       // _bandera = false
        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        
        

//    let ob = EncuestaObject(_id: _id, cNombreEncuesta: self.cNombreEncuesta, lstRespuestas: self.arrayQuestionsSend, nIdConvencion: con)
        
        //let jd = ""
        var jsonString = ""
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(arrayQuestionsSend)
            
            jsonString = String(data: jsonData, encoding: .utf8)!
        }
        catch {
            
            let message = MDCSnackbarMessage()
            message.text = ":("
            MDCSnackbarManager.show(message)
            self.navigationController?.popViewController(animated: true)
        }
        
        
        let params:Parameters = ["_id":_id, "cNombreEncuesta":cNombreEncuesta, "lstRespuestas":jsonString, "nIdConvencion": con]
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        print(params)
        
        Alamofire.request(JSONKey.URL_ENCUESTA, method: .post, parameters: params, headers: headers).responseJSON{
            res in
    
            if let JSON = res.result.value as? NSDictionary{
                
                let message = MDCSnackbarMessage()
                message.text = "Respuesta Enviada."
                MDCSnackbarManager.show(message)
                self.navigationController?.popViewController(animated: true)
            }else{
                
                let message = MDCSnackbarMessage()
                message.text = "Lo sentimos, algo salio mal."
                MDCSnackbarManager.show(message)
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        }else{
            
            print(index?.row)
        }
    }
}
extension QuickViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrayQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellQuick", for: indexPath)
        
        cell.imageView?.image = UIImage(named: "ic_radio_button_unchecked")
        cell.textLabel?.text = arrayQuestions[indexPath.row].cRespuesta
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell = tableView.cellForRow(at: indexPath)!
//        selectedCell.textLabel?.textColor = UIColor.white
//        selectedCell.contentView.backgroundColor = UIColor(red: 92/255, green: 182/255, blue: 240/255, alpha: 1.0)
        selectedCell.imageView?.image = UIImage(named: "radio-on-button_pink")
        btnSend.isEnabled = true
        btnSend.backgroundColor = UIColor(red: 28/255, green: 136/255, blue: 233/255, alpha: 1.0)
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath)!
//        selectedCell.textLabel?.textColor = UIColor.black
//        selectedCell.contentView.backgroundColor = UIColor.white
        selectedCell.imageView?.image = UIImage(named: "ic_radio_button_unchecked")
    }
}


