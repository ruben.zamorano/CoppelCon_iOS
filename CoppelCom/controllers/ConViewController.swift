//
//  ConViewController.swift
//  CoppelCom
//
//  Created by MacBook Pro on 28/11/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import UIKit

class ConViewController: UIViewController {

    let sessionser = UserDefaults.standard
    
    
    var idNacional = ""
    var idRegional = ""
    
    @IBOutlet weak var imgRegional: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func actionNacional(_ sender: UIButton) {
    }
    @IBOutlet weak var btnNacional: UIButton!
    @IBOutlet weak var actionRegional: UIButton!
    @IBAction func actionRegional(_ sender: UIButton) {
    }

    override func viewDidAppear(_ animated: Bool) {
        
        if sessionser.object(forKey: JSONKey.KEY_CON) != nil{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "homeController") as! HomeViewController
            navigationController?.pushViewController(vc,animated: true)
            
            
        }
        
        if sessionser.object(forKey: JSONKey.KEY_CON_NAC) == nil{
            
            btnNacional.isEnabled = false
            
        }else{
            print("session user NAC: \(sessionser.object(forKey: JSONKey.KEY_CON_NAC) as! String)")
            btnNacional.isEnabled = true
            
        }
        
        if sessionser.object(forKey: JSONKey.KEY_CON_REG) == nil{
            
            imgRegional.image = UIImage(named:"nonConvention")
            actionRegional.setImage(UIImage(named:"nonConvention"), for: .normal)
            actionRegional.isEnabled = false
            
        }else{
            print("session user REG: \(sessionser.object(forKey: JSONKey.KEY_CON_REG) as! String)")
            actionRegional.isEnabled = true
            
            
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "passNacional"{
            
            let conNac = sessionser.object(forKey: JSONKey.KEY_CON_NAC) as! String
            sessionser.set(conNac, forKey: JSONKey.KEY_CON)
//            sessionser.set(conNac, forKey: JSONKey.KEY_CON_NAC)
            sessionser.synchronize()
            
        }else{
            
            let conNac = sessionser.object(forKey: JSONKey.KEY_CON_REG) as! String
            sessionser.set(conNac, forKey: JSONKey.KEY_CON)
            sessionser.synchronize()
        }
    }
}
