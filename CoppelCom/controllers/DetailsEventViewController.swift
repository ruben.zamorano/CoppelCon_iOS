//
//  DetailsEventViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 10/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MaterialSnackbar

class DetailsEventViewController: UIViewController {

    @IBOutlet weak var progressLoad: UIActivityIndicatorView!
    @IBOutlet weak var lblLugar: UILabel!
    @IBOutlet weak var imgPonent: UIImageView!
    @IBOutlet weak var dateShadow: UIView!
    @IBOutlet weak var ponentDetailsClik: UIView!
    
    @IBOutlet weak var tvSubPonents: UITableView!
    
    @IBOutlet weak var lblDatePresent: UILabel!
    @IBOutlet weak var lblTitlePresent: UILabel!
    
    @IBOutlet weak var lblHourPresent: UILabel!
    @IBOutlet weak var lblPlacePresent: UILabel!
    
    @IBOutlet weak var lblTableNumberPresent: UILabel!
    
    @IBOutlet weak var lblCodePresent: UILabel!
    @IBOutlet weak var lblNamePonent: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    let sessionUser = UserDefaults.standard
    
    
    
    var arraySubPonents:[PonentObject] = []
    var present = File()
    var ponent = PonentObject()
    var GETmesa = ""
    var ref: DatabaseReference = Database.database().reference()

    
    override func viewDidAppear(_ animated: Bool) {
        
        let convertImage = ConvertBase64.DecodingURLPicture(image: "\(JSONKey.URL_IMAGE_BASE)\(present.img)")
        imgPonent.image = UIImage(data: convertImage as Data)
       
        
        self.progressLoad.stopAnimating()
        self.progressLoad.isHidden = true
        
        self.tableViewHeight.constant = CGFloat(90 * self.arraySubPonents.count)
        self.tvSubPonents.frame.size.height = CGFloat(90 * self.arraySubPonents.count)
        self.tvSubPonents.reloadData()
        Timer.scheduledTimer(timeInterval: 1 , target: self, selector: #selector(self.update), userInfo: nil, repeats: false);
        
        print("File-----")
        print(present.codigo)
        print(present.descripcion)
        print(present.hora)
        print(present.id)
        print(GETmesa)
        print(present.titulo)
        
    }
    
    @objc func update() {
        
        //scrollView.backgroundColor = UIColor.red
        
        let suma = 330 + ponentDetailsClik.frame.height + tvSubPonents.frame.height + lblPlacePresent.frame.height + lblHourPresent.frame.height + lblTableNumberPresent.frame.height + lblCodePresent.frame.height
        
        //tvSubPonents.frame.size.height = 19 * 4
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: suma)
        scrollView.layoutIfNeeded()
        scrollView.clipsToBounds = true
        
        print(arraySubPonents)
        //self.alert.dismiss(animated: true, completion: nil)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgPonent.layer.borderWidth = CGFloat(Float(4))
        imgPonent.layer.borderColor = UIColor(red: 28/255, green: 136/255, blue: 233/255, alpha: 1.0).cgColor
        imgPonent.layer.cornerRadius = imgPonent.frame.size.width / 2
        imgPonent.clipsToBounds = true
        
        self.progressLoad.startAnimating()
        self.progressLoad.isHidden = false
        
        if !Reachability.isConnectedToNetwork(){
            
            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            
            self.progressLoad.stopAnimating()
            self.progressLoad.isHidden = true
            
        }
        
        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        
        ref.child(con).child("ponentes").child("\(present.idPonente)").observe(DataEventType.value, with: { (snapshot) in
            
            
            
            if let postDict = snapshot.value as? NSDictionary{
            
                
            // MARK- Get data to show Display
            self.lblTitlePresent.text = postDict.value(forKey: JSONKey.KEY_C_TEMAPRESENTACION_PONENTE) as? String ?? ""
            self.lblNamePonent.text = postDict.value(forKey: JSONKey.KEY_C_NOMBREPONENTE) as? String ?? ""
                self.lblLugar.text = "Lugar: \(self.present.lugar)"
                
           //     let hora = self.present.hora//postDict.value(forKey: "cHora") as? String ?? "---"
//            let horaFin = self.present. //postDict.value(forKey: "cHoraFin") as? String ?? "---"
           // self.lblDatePresent.text = "Hora inicio: \(hora) - Hora fin:\(horaFin)"
                
                
            let desc = postDict.value(forKey: JSONKey.KEY_C_DESCRIPCION) as? String ?? ""
            //self.lblPlacePresent.attributedText = desc.convertHtml()
                
                
                
                self.lblPlacePresent.text = self.present.titulo
                
           

            
            //MARK- Asign to ponent Object
            self.ponent.descripcion = desc
            self.ponent.codigoVestimenta = postDict.value(forKey: JSONKey.KEY_C_CODIGOVESTIMENTA) as? String ?? ""
            self.ponent.hora = postDict.value(forKey: JSONKey.KEY_C_HORA) as? String ?? ""
            self.ponent.mesa = postDict.value(forKey: JSONKey.KEY_C_MESA) as? String ?? ""
            self.ponent.nombre = postDict.value(forKey: JSONKey.KEY_C_NOMBREPONENTE) as? String ?? ""
            self.ponent.tema = postDict.value(forKey: JSONKey.KEY_C_TEMAPRESENTACION_PONENTE) as? String ?? ""
            self.ponent.ubicacion = postDict.value(forKey: JSONKey.KEY_C_UBICACION) as? String ?? ""
            print(self.present.img)
                if(self.present.img != ""){
                    
                    if Reachability.isConnectedToNetwork(){
                    
    let image = ConvertBase64.DecodingURLPicture(image: "\(JSONKey.URL_IMAGE_BASE)\(self.present.img)")
                        
                            self.ponent.img = UIImage(data: image as Data)!
                    }else{
                        
                        self.ponent.img = UIImage(named: "ic_person")!
                    }
                }else{
                    
                    self.ponent.img = UIImage(named: "ic_person")!
                }
                
                
                
                
            // MARK- Get Subponents
                let arrayP = self.present.lstSubPonents
                if arrayP.count > 0{
                
                for i in arrayP{
                    
                    self.ref.child(con).child("ponentes").child("\(i)").observe(DataEventType.value, with: { (snapshot) in
                        
                        if let reqSubPon = snapshot.value as? NSDictionary{
                            
                            
                            let subPon = PonentObject()
                            
                            subPon.tema = reqSubPon.value(forKey: JSONKey.KEY_C_TEMAPRESENTACION_PONENTE) as? String ?? ""
                            subPon.ubicacion = reqSubPon.value(forKey: JSONKey.KEY_C_UBICACION) as? String ?? ""
                            subPon.codigoVestimenta = reqSubPon.value(forKey: JSONKey.KEY_C_CODIGOVESTIMENTA) as? String ?? ""
                            subPon.descripcion = reqSubPon.value(forKey: JSONKey.KEY_C_DESCRIPCION) as? String ?? ""
                            subPon.hora = reqSubPon.value(forKey: JSONKey.KEY_C_HORA) as? String ?? ""
                            subPon.mesa = reqSubPon.value(forKey: JSONKey.KEY_C_MESA) as? String ?? ""
                            subPon.nombre = reqSubPon.value(forKey: JSONKey.KEY_C_NOMBREPONENTE) as? String ?? ""
                            let reqImg = reqSubPon.value(forKey: JSONKey.KEY_C_IMAGEN) as? String ?? ""
                            
                            if(reqImg != "" ){
                                
                                
                                if Reachability.isConnectedToNetwork(){
                                    
                                    let image = ConvertBase64.DecodingURLPicture(image: "\(JSONKey.URL_IMAGE_BASE)\(reqImg)")
                                    subPon.img = UIImage(data: image as Data)!
                                    
                                }else{
                                    self.ponent.img = UIImage(named: "ic_person")!
                                    
                                }
                                
                                
                            }else{
                                self.progressLoad.stopAnimating()
                                self.progressLoad.isHidden = true
                                subPon.img = UIImage(named: "ic_person")!
                            }
                            
                            self.arraySubPonents.append(subPon)
                        }else{
                            self.progressLoad.stopAnimating()
                            self.progressLoad.isHidden = true
                            print("nopo")
                        }
                        
                    })
                }
                    
                    
                }
                
            }else{
                
                self.progressLoad.stopAnimating()
                self.progressLoad.isHidden = true
                let message = MDCSnackbarMessage()
                message.text = "Ponente no encontrado."
                MDCSnackbarManager.show(message)
                
            }
        })
        
        
        lblHourPresent.text = "Hora: \(present.hora)"
        lblTableNumberPresent.text = "#\(GETmesa)"
        lblCodePresent.text = "Código de vestimenta: \(present.codigo)"
        

        
        //if(present.img != ""){
          
        
            
        //}else{
            
          //  imgPonent.image = UIImage(named: "ic_person")
        //}
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.someAction))
        ponentDetailsClik.addGestureRecognizer(gesture)
        
        
        
        self.ponentDetailsClik.layer.masksToBounds = false
        self.ponentDetailsClik.layer.shadowColor = UIColor.black.cgColor
        self.ponentDetailsClik.layer.shadowOpacity = 0.5
        self.ponentDetailsClik.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.ponentDetailsClik.layer.shadowRadius = 1
        
        
        
        self.dateShadow.layer.masksToBounds = false
        self.dateShadow.layer.shadowColor = UIColor.black.cgColor
        self.dateShadow.layer.shadowOpacity = 0.5
        self.dateShadow.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.dateShadow.layer.shadowRadius = 1
        
        
       
        
    }
        @objc func someAction() {
    
            if(ponent.nombre != ""){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "detailsPonentController") as! DetailsPonentViewController
            print(ponent.img)
            vc.ponentDetails = ponent
            //vc.newsObj = newsObj
            navigationController?.pushViewController(vc,animated: true)
            }else{
                
                let message = MDCSnackbarMessage()
                message.text = "Ponente no encontrado."
                MDCSnackbarManager.show(message)
                
            }
        }

}
extension DetailsEventViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySubPonents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubPonents", for: indexPath) as! CustomTableViewCell
    
        print("------>>> \(arraySubPonents[indexPath.row].nombre)")
        cell.lblNamePonentDetails.text = arraySubPonents[indexPath.row].nombre
        cell.imgPonentDetails.image = arraySubPonents[indexPath.row].img
        
        
        cell.imgPonentDetails.layer.borderWidth = CGFloat(Float(3))
        cell.imgPonentDetails.layer.borderColor = UIColor(red: 28/255, green: 136/255, blue: 233/255, alpha: 1.0).cgColor
        cell.imgPonentDetails.layer.cornerRadius = cell.imgPonentDetails.frame.width/2
        cell.imgPonentDetails.clipsToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}
