//
//  HotelViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 10/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import MaterialComponents.MaterialSnackbar

class HotelViewController: UIViewController {

    
    
    
    @IBOutlet weak var lblEmployename: UILabel!
    @IBOutlet weak var lblWifeName: UILabel!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblRoomNumber: UILabel!
    @IBOutlet weak var lblEnterDay: UILabel!
    @IBOutlet weak var lblexitDay: UILabel!
    @IBOutlet weak var lblTypeRoom: UILabel!
    
    var ref: DatabaseReference = Database.database().reference()

    
    let sessionUser = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblEmployename.text = ""
        self.lblexitDay.text = ""
        self.lblHotelName.text = ""
        self.lblTypeRoom.text = ""
        self.lblEnterDay.text = ""
        self.lblexitDay.text = ""
        self.lblWifeName.text = ""
        self.lblRoomNumber.text = ""
        if !Reachability.isConnectedToNetwork(){
            
            let message = MDCSnackbarMessage()
            message.text = "Sin conexion a internet."
            MDCSnackbarManager.show(message)
            
        }
            

        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
        let idUser = sessionUser.object(forKey: JSONKey.KEY_ID_USER) as! String
        
        ref.child(con).child("usuarios").child(idUser).observe( .value, with: { snapshat in
            
            if let JSON = snapshat.value as? NSDictionary{
            
                print(JSON)
                if let hotel = JSON.value(forKey: "hotel") as? NSDictionary {
                    //let nombreEmpleado = hotel.value(forKey: "cNombreEmpleado") as? String
                    
                    //if(nombreEmpleado == "ROBERTO GUTIERREZ RUELAS"){
                        
                        self.lblEmployename.text = hotel.value(forKey: JSONKey.KEY_C_NOMBRE_EMPLEADO) as? String
                        self.lblexitDay.text = hotel.value(forKey: JSONKey.KEY_C_SALIDA) as? String
                        self.lblHotelName.text = hotel.value(forKey: JSONKey.KEY_C_NOOMBRE_HOTEL) as? String
                        self.lblTypeRoom.text = hotel.value(forKey: JSONKey.KEY_C_TIPO_HABITACION) as? String
                        self.lblEnterDay.text = hotel.value(forKey: JSONKey.KEY_C_ENTRADA) as? String
                        self.lblexitDay.text = hotel.value(forKey: JSONKey.KEY_C_SALIDA) as? String
                self.lblRoomNumber.text = "Habitación: #\(hotel.value(forKey: JSONKey.KEY_C_HABITACION) as? String ?? "")"
                        if(hotel.value(forKey: JSONKey.KEY_B_PAREJA) as? Bool ?? false){
                        
                           // let parnerObject = hotel.value(forKey: JSONKey.KEY_PAREJAS) as? NSDictionary
                        
                            self.lblWifeName.text = self.sessionUser.object(forKey: JSONKey.KEY_B_PAREJA) as? String ?? "Sin pareja"
                        
                        }else{
                        
                            self.lblWifeName.text = "Sin acompañante"
                        }
                        
                }else{
                
                let message = MDCSnackbarMessage()
                message.text = "Sin registro de hoteles."
                MDCSnackbarManager.show(message)
            }
            

            }else{
                
                let message = MDCSnackbarMessage()
                message.text = "Sin registro de hoteles."
                MDCSnackbarManager.show(message)
                
            }
        })
        
        
     
    }
}
