//
//  NotificationViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 17/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit
import CoreData

class NotificationViewController: UIViewController {

    var window: UIWindow?
    var root = false
    
    @IBOutlet weak var tvNotifications: UITableView!
    
    var arrayNot:[NotificationObject] = []
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("ROOOOT ---> \(root)")
        getData()
        inicializaciones()
        
        
    }
    
    func getData(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notify")
        request.returnsObjectsAsFaults = false
        
        do{
            
            let result = try context.fetch(request)
            
            if result.count > 0{
                
                
                
                for r in result as! [NSManagedObject]{
                    
                    
                    let newNot = NotificationObject()
                    
                    newNot.title = r.value(forKey: JSONKey.KEY_TITLE) as? String ?? ""
                    newNot.action = r.value(forKey: JSONKey.KEY_ACTION) as! String
                    newNot.msg = r.value(forKey: JSONKey.KEY_MSG) as! String
                    newNot.status = r.value(forKey: JSONKey.KEY_STATUS) as! Bool
                    
                    print(newNot.status)
                    self.arrayNot.append(newNot)
                }
                
                
                tvNotifications.reloadData()
                
            }else{
                
                print("BD_empty[X02]")
            }
            
        }catch{
            
            print("Catch[X01]")
        }
        
    }

    func inicializaciones(){
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh) , for: .valueChanged)
        tvNotifications.addSubview(refreshControl)
        
    }

    @IBAction func actionClose(_ sender: UIButton) {
        
        //dismiss(animated: true, completion: nil)
        
        if(root){
        print("entro aqui")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.closeWindow()
            //window?.rootViewController?.dismiss(animated: true, completion: nil)
            
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
        //

    }
    @objc func refresh() {
        
        arrayNot.removeAll()
        getData()
        tvNotifications.reloadData()
        refreshControl.endRefreshing()
    }
}
extension NotificationViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayNot.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNot", for: indexPath) as! CustomTableViewCell
        
        //print(arrayNot[indexPath.row].msg)
        cell.lbltitleNotification.text = arrayNot[(arrayNot.count-1) - indexPath.section].msg
        cell.lblDateNotification.text = arrayNot[(arrayNot.count-1) - indexPath.section].title
        
        cell.pointView.layer.cornerRadius = cell.pointView.frame.width / 2
        cell.pointView.clipsToBounds = true
        
        if(!arrayNot[(arrayNot.count-1) - indexPath.section].status){
            
            cell.pointView.backgroundColor = UIColor(red: 245/255, green: 75/255, blue: 127/255, alpha: 1.0)
            cell.backgroundColor = UIColor(red: 214/255, green: 242/255, blue: 238/255, alpha: 1.0)
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notify")
        request.returnsObjectsAsFaults = false
        
        do{
            
            let result = try context.fetch(request)
            
            if result.count > 0{
                
                
                
                for r in result as! [NSManagedObject]{
                    
                    
                    if(arrayNot[(arrayNot.count-1) - indexPath.section].title == r.value(forKey: JSONKey.KEY_TITLE) as? String ?? ""){
                        
                        print(arrayNot[(arrayNot.count-1) - indexPath.section].msg)
                        print(arrayNot[(arrayNot.count-1) - indexPath.section].status)
                        
                        r.setValue(true, forKey: JSONKey.KEY_STATUS)
                        
                        navigationController?.popViewController(animated: true)
                        do{
                            
                            try context.save()
                            self.dismiss(animated: true, completion: nil)
                            
                        }catch{
                            
                            print("ErrorUpdate[00x1]")
                        }
                    }
                    
                }
                
                
                tvNotifications.reloadData()
                
            }else{
                
                print("BD_empty[X02]")
            }
            
        }catch{
            
            print("Catch[X01]")
        }
    }
    
}
