//
//  DetailsEventsViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 11/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit

class DetailsEventsViewController: UIViewController {

    var event = File()
    var GETmesa = ""
    
    @IBOutlet weak var backLogoSLP: UIView!
    @IBOutlet weak var logoSLP: UIImageView!
    @IBOutlet weak var imgEventDetails: UIImageView!
    @IBOutlet weak var lblDescEventDetails: UILabel!
    @IBOutlet weak var lblPlaceEventDetails: UILabel!
    @IBOutlet weak var lblHourEventDetails: UILabel!
    @IBOutlet weak var lblNumberEventDetails: UILabel!
    @IBOutlet weak var lblCodeVestEventDetails: UILabel!
    
    @IBOutlet weak var imgFooter: UIImageView!
    
    @IBOutlet weak var svDetails: UIScrollView!
    @IBOutlet weak var bvDetails: UIView!
    
    
    var alert = UIAlertController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        alert = UIAlertController(title: "Espere un momento.", message: "Cargando datos", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        print(event.descripcion)
        print(event.titulo)
        
        let html = event.descripcion
        let data = html.data(using: .utf8)!
        let att = try! NSAttributedString.init(
            data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        let matt = NSMutableAttributedString(attributedString:att)
        matt.enumerateAttribute(
            NSAttributedStringKey.font,
            in:NSMakeRange(0,matt.length),
            options:.longestEffectiveRangeNotRequired) { value, range, stop in
                let f1 = value as! UIFont
                let f2 = UIFont(name:"Georgia", size:20)!
                if let f3 = applyTraitsFromFont(f1, to:f2) {
                    matt.addAttribute(
                        NSAttributedStringKey.font, value:f3, range:range)
                }
        }
        
        lblDescEventDetails.attributedText =  matt//event.descripcion.convertHtml()//event.descripcion
        
        lblPlaceEventDetails.text = "Lugar: \(event.lugar)"
        lblHourEventDetails.text = "Hora: \(event.hora)"
        lblNumberEventDetails.text = "# \(GETmesa)"
        lblCodeVestEventDetails.text = "Codigo de vestimenta: \(event.codigo)"
        
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        Timer.scheduledTimer(timeInterval: 1 , target: self, selector: #selector(self.update), userInfo: nil, repeats: false);
    }

    @objc func update() {
        
       
        let suma = 280 + lblCodeVestEventDetails.frame.height + lblNumberEventDetails.frame.height + lblHourEventDetails.frame.height + lblPlaceEventDetails.frame.height + lblDescEventDetails.frame.height
        
        if((suma + 150) >= view.frame.height){
            
            imgFooter.frame.origin.y = (suma + 150) - 32
            
        }else{
            
            imgFooter.frame.origin.y = view.frame.height - 97
            
        }
        
        
        print(suma)
        print("label")
        print(self.lblDescEventDetails.frame.height)

        
        self.svDetails.contentSize = CGSize(width: self.view.frame.width, height: suma + 150)
        self.alert.dismiss(animated: true, completion: nil)
        
        if("" == event.img){
            
            imgEventDetails.frame.size.width = 200
            imgEventDetails.frame.origin.x = (view.frame.width/2) - 100
            imgEventDetails.image = UIImage(named: "no_image")
            
        }else{
            //print(event.img)
            let convertImage = ConvertBase64.DecodingURLPicture(image: "\(JSONKey.URL_IMAGE_BASE)\(event.img)" )
            imgEventDetails.image = UIImage(data: convertImage as Data)
        }
        
        backLogoSLP.isHidden = true
    }
    func applyTraitsFromFont(_ f1: UIFont, to f2: UIFont) -> UIFont? {
        let t = f1.fontDescriptor.symbolicTraits
        if let fd = f2.fontDescriptor.withSymbolicTraits(t) {
            return UIFont.init(descriptor: fd, size: 0)
        }
        return nil
    }
}
extension String{
    func convertHtml() -> NSAttributedString{

        guard let data = data(using: .utf8 ) else { return NSAttributedString() }
        do{

//            let at = NSAttributedString()
//            at.attribute(.font:UIFont(24.0), at: 0, effectiveRange: "")

            return try NSAttributedString(data: data ,options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue ], documentAttributes: nil)
        }catch{
            return NSAttributedString()

        }
    }
    
    func applyTraitsFromFont(_ f1: UIFont, to f2: UIFont) -> UIFont? {
        let t = f1.fontDescriptor.symbolicTraits
        if let fd = f2.fontDescriptor.withSymbolicTraits(t) {
            return UIFont.init(descriptor: fd, size: 0)
        }
        return nil
    }
}

