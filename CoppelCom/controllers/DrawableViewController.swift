//
//  DrawableViewController.swift
//  CoppelCom
//
//  Created by MacBook Pro on 13/12/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import UIKit
import CoreData
import MaterialComponents.MaterialSnackbar

class DrawableViewController: UIViewController{
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgFrame: UIImageView!
    @IBOutlet weak var segmenteFrame: UISegmentedControl!
    
    
    @IBOutlet weak var frUno: UIView!
    @IBOutlet weak var frDos: UIView!
    @IBOutlet weak var frTres: UIView!
    
    
    public var img = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgView.image = img
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        segmenteFrame.tintColor = UIColor.clear
        segmenteFrame.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black, (NSAttributedStringKey.font as NSCopying) as! AnyHashable:UIFont(name: "Helvetica Neue", size: 15.0)!], for: UIControlState.normal)
    }
    
    @IBAction func actionSegmentFrame(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            
            frUno.backgroundColor = UIColor(red: 245/255, green: 75/255, blue: 127/255, alpha: 1.0)
            frDos.backgroundColor = UIColor.white
            frTres.backgroundColor = UIColor.white
            
            imgFrame.image = UIImage(named:"marco1")
            
            break
        case 1:
            
            frDos.backgroundColor = UIColor(red: 245/255, green: 75/255, blue: 127/255, alpha: 1.0)
            frUno.backgroundColor = UIColor.white
            frTres.backgroundColor = UIColor.white
            
            imgFrame.image = UIImage(named:"marco2")
            
            break
        case 2:
            
            frTres.backgroundColor = UIColor(red: 245/255, green: 75/255, blue: 127/255, alpha: 1.0)
            frDos.backgroundColor = UIColor.white
            frUno.backgroundColor = UIColor.white
            
            imgFrame.image = UIImage(named:"marco3")
            
            break
            
        default:
            break
        }
        
    
    }
    
    @IBAction func actionSelect(_ sender: UIButton) {
        
        if(imgView.image != nil){
        let bottomImage = imgView.image!//UIImage(named: "caoruselOne")!
        
        //imgView.frame.size.height = bottomImage.size.height
        //imgView.frame.size.width = bottomImage.size.width
        
        let topImage = imgFrame.image!//UIImage(named: "marco1")!
        //topImage.draw(in: CGRect(x: 0, y: 0, width: bottomImage.size.width, height: 69))
        
        let newSize = CGSize(width: 375, height: 375)
        
        //let newSize2 = CGSize(width: 375, height: 375)  // set this to what you need
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        
        bottomImage.draw(in: CGRect(origin: .zero , size: newSize))
        topImage.draw(in: CGRect(origin: .zero, size: newSize))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //        img.image = newImage
        
        
        
        let photo = UIImagePNGRepresentation(newImage!)
        let compresedImage = UIImage(data: photo!)
        UIImageWriteToSavedPhotosAlbum(compresedImage!, nil, nil, nil)
        
        
            
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let contx = appDelegate.persistentContainer.viewContext
            let newPhoto = NSEntityDescription.insertNewObject(forEntityName: "Photos", into: contx)
            
            
            let dataNs = ConvertBase64.EncodingBase64(image: compresedImage!)
            
            newPhoto.setValue(dataNs, forKey: "photo")
            
            
            do{
                
                try contx.save()
                print("SAVED")
                
            }catch{
                
                print("NO SAVED")
                
            }
 
            
        imgView.image = UIImage()
        imgFrame.image = UIImage(named:"marco1")
            
            self.dismiss(animated: true, completion: nil)
            
        }else{
            
            let message = MDCSnackbarMessage()
            message.text = "Imagen vacia."
            MDCSnackbarManager.show(message)
            
        }
        
    }
    @IBAction func actionAtras(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

