//
//  TraslatesViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 10/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//
//
//import UIKit
//import Alamofire
//import Firebase
//import MaterialComponents.MaterialSnackbar
//class TraslatesViewController: UIViewController {
//
//    @IBOutlet weak var segmentedDay: UISegmentedControl!
//
//    @IBOutlet weak var fSab: UIView!
//    @IBOutlet weak var fVier: UIView!
//    @IBOutlet weak var fDom: UIView!
//
//
//    @IBOutlet weak var cvTransportSabado: UICollectionView!
//    @IBOutlet weak var cvtransport: UICollectionView!
//
//    @IBOutlet weak var cvTransportDomingo: UICollectionView!
//
//    @IBOutlet weak var fragmentUno: UICollectionView!
//    @IBOutlet weak var fragmentDos: UIView!
//    @IBOutlet weak var fragmentThree: UIView!
//
//    var arrayTraslates:[String] = []
//    var arrayTraslatesSabado:[String] = []
//    var arrayTraslatesDomingo:[String] = []
//
//    var arrayBuses = [[BusesObject]]()
//    var arrayBusesSabado = [[BusesObject]]()
//    var arrayBusesDomingo = [[BusesObject]]()
//
//    var ref: DatabaseReference = Database.database().reference()
//
//
//
//
//    let sessionUser = UserDefaults.standard
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//        if !Reachability.isConnectedToNetwork(){
//
//            let message = MDCSnackbarMessage()
//            message.text = "Sin conexion a internet."
//            MDCSnackbarManager.show(message)
//
//        }
//
//        let con = sessionUser.object(forKey: JSONKey.KEY_CON) as! String
//
//        ref.child(con).child("hoteles").child("traslados").observe(DataEventType.value, with: { (snapshot) in
//
//            if let JSON = snapshot.value as? NSArray{
//
//                self.arrayBusesDomingo.removeAll()
//                self.arrayBuses.removeAll()
//                self.arrayBusesSabado.removeAll()
//                self.arrayTraslates.removeAll()
//                self.arrayTraslatesSabado.removeAll()
//                self.arrayTraslatesDomingo.removeAll()
//
//                for val in JSON {
//                    let dia = (val as AnyObject).value(forKey: JSONKey.KEY_C_DIA) as! String
//
//
//                    if(dia == "Viernes")
//                    {
//
//                        let arraySalidas = (val as AnyObject).value(forKey: JSONKey.KEY_LST_TRASLADOS) as! NSArray
//
//                        for item in arraySalidas{
//
//                            let arrayGetBus = (item as AnyObject).value(forKey: JSONKey.KEY_LST_CAMIONES) as! NSArray
//                            let sal = (item as AnyObject).value(forKey: JSONKey.KEY_C_SALIDA) as! String
//
//                            self.arrayTraslates.append(sal)
//
//                            var subBus:[BusesObject] = []
//
//                            for i in arrayGetBus{
//
//                                let buses = BusesObject()
//                                buses.bus = (i as AnyObject).value(forKey: JSONKey.KEY_C_TRANSPORTE) as! String
//                                buses.hour = (i as AnyObject).value(forKey: JSONKey.KEY_C_HORA) as! String
//
//                                subBus.append(buses)
//                                //self.arrayTraslates.append(buses)
//                            }
//
//                            self.arrayBuses.append(subBus)
//
//                        }
//                        self.cvtransport.reloadData()
//
//                    }else if(dia == "Sabado")
//                    {
//
//                        let arraySalidas = (val as AnyObject).value(forKey: JSONKey.KEY_LST_TRASLADOS) as! NSArray
//
//                        for item in arraySalidas{
//
//                            let arrayGetBus = (item as AnyObject).value(forKey: JSONKey.KEY_LST_CAMIONES) as! NSArray
//                            let sal = (item as AnyObject).value(forKey: JSONKey.KEY_C_SALIDA) as! String
//
//                            self.arrayTraslatesSabado.append(sal)
//
//                            var subBus:[BusesObject] = []
//
//                            for i in arrayGetBus{
//
//                                let buses = BusesObject()
//                                buses.bus = (i as AnyObject).value(forKey: JSONKey.KEY_C_TRANSPORTE) as! String
//                                buses.hour = (i as AnyObject).value(forKey: JSONKey.KEY_C_HORA) as! String
//
//                                subBus.append(buses)
//                                //self.arrayTraslates.append(buses)
//                            }
//
//                            self.arrayBusesSabado.append(subBus)
//
//                        }
//                        self.cvTransportSabado.reloadData()
//                    }else if(dia == "Domingo")
//                    {
//                        print("DOMINGO")
//                        let arraySalidas = (val as AnyObject).value(forKey: JSONKey.KEY_LST_TRASLADOS) as! NSArray
//
//                        for item in arraySalidas{
//
//                            let arrayGetBus = (item as AnyObject).value(forKey: JSONKey.KEY_LST_CAMIONES) as! NSArray
//                            let sal = (item as AnyObject).value(forKey: JSONKey.KEY_C_SALIDA) as! String
//
//                            self.arrayTraslatesDomingo.append(sal)
//
//                            var subBus:[BusesObject] = []
//
//                            for i in arrayGetBus{
//
//                                let buses = BusesObject()
//                                buses.bus = (i as AnyObject).value(forKey: JSONKey.KEY_C_TRANSPORTE) as! String
//                                buses.hour = (i as AnyObject).value(forKey: JSONKey.KEY_C_HORA) as! String
//
//                                subBus.append(buses)
//                                //self.arrayTraslates.append(buses)
//                            }
//
//                            self.arrayBusesDomingo.append(subBus)
//
//                        }
//                        self.cvTransportDomingo.reloadData()
//                    }
//
//                }
//
//
//
//
//            }else{
//                let message = MDCSnackbarMessage()
//                message.text = "No hay traslados por el momento."
//                MDCSnackbarManager.show(message)
//            }
//
//        })
//
//
//
//    }
//    override func viewDidAppear(_ animated: Bool) {
//
//
//        print(arrayBusesDomingo)
//        segmentedDay.tintColor = UIColor.clear
//        segmentedDay.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white, (NSAttributedStringKey.font as NSCopying) as! AnyHashable:UIFont(name: "didonesque", size: 15.0)!], for: UIControlState.normal)
//
//
//    }
//
//    @IBAction func actionSegmented(_ sender: UISegmentedControl) {
//
//        switch sender.selectedSegmentIndex {
//        case 0:
//            fVier.backgroundColor = UIColor.white
//            fSab.backgroundColor = UIColor.clear
//            fDom.backgroundColor = UIColor.clear
//
//            fragmentUno.isHidden = false
//            fragmentDos.isHidden = true
//            fragmentThree.isHidden = true
//            break
//        case 1:
//
//            fSab.backgroundColor = UIColor.white
//            fVier.backgroundColor = UIColor.clear
//            fDom.backgroundColor = UIColor.clear
//
//            fragmentThree.isHidden = true
//            fragmentUno.isHidden = true
//            fragmentDos.isHidden = false
//
//            break
//        case 2:
//
//            fDom.backgroundColor = UIColor.white
//            fSab.backgroundColor = UIColor.clear
//            fVier.backgroundColor = UIColor.clear
//
//
//            fragmentUno.isHidden = true
//            fragmentDos.isHidden = true
//            fragmentThree.isHidden = false
//
//            break
//        default:
//            break
//        }
//    }
//    var ban = 0
//    var ban_dos = 0
//}
//extension TraslatesViewController: UITableViewDelegate, UITableViewDataSource{
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print(tableView.tag)
//
//        var numberSec = 0
//
//        if(tableView.tag < 3){
//
//            numberSec = arrayBuses[tableView.tag].count
//
//        }else if(tableView.tag < 14 && tableView.tag > 3){
//
//            numberSec = arrayBusesSabado[tableView.tag - 10].count
//
//        }else {
//
//            numberSec = arrayBusesDomingo[tableView.tag - 20].count
//        }
//
//        return numberSec
//    }
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        var cellBus = CustomTableViewCell()
//
//        if(tableView.tag < 3){
//
//            print(tableView.tag)
//            cellBus = tableView.dequeueReusableCell(withIdentifier: "cellBus", for: indexPath) as! CustomTableViewCell
//
//            cellBus.lblBus.text = arrayBuses[tableView.tag][indexPath.row].bus
//            cellBus.lblHora.text = arrayBuses[tableView.tag][indexPath.row].hour
//
//        }else if(tableView.tag < 14 && tableView.tag > 3){
//
//            cellBus = tableView.dequeueReusableCell(withIdentifier: "cellBusSabado", for: indexPath) as! CustomTableViewCell
//
//            cellBus.lblBusSabado.text = arrayBusesSabado[tableView.tag - 10][indexPath.row].bus
//            cellBus.lblHoraSabado.text = arrayBusesSabado[tableView.tag - 10][indexPath.row].hour
//        }else{
//
//            cellBus = tableView.dequeueReusableCell(withIdentifier: "cellBusDomingo", for: indexPath) as! CustomTableViewCell
//
//            cellBus.lblBusDomingo.text = arrayBusesDomingo[tableView.tag - 20][indexPath.row].bus
//            cellBus.lblHoraDomingo.text = arrayBusesDomingo[tableView.tag - 20][indexPath.row].hour
//        }
//
//
//        return cellBus
//    }
//
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return   19.0
//
//    }
//
//}
//extension TraslatesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        var numberTras = 0
//
//        if(collectionView == cvtransport)
//        {
//            numberTras = arrayTraslates.count
//
//        }else if(collectionView == cvTransportSabado){
//
//            numberTras = arrayTraslatesSabado.count
//        }else{
//
//            numberTras = arrayTraslatesDomingo.count
//
//        }
//
//        return numberTras
//    }
//    //itemDomingo
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        var cell = CustomCollectionViewCell()
//
//        if(collectionView == cvtransport){
//
//            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemTransporte", for: indexPath) as! CustomCollectionViewCell
//
//            cell.lblExit.text = arrayTraslates[indexPath.row]
//            cell.tvBuses.tag = indexPath.row
//            cell.tvBuses.isUserInteractionEnabled = false
//
//        }else if(collectionView == cvTransportSabado){
//
//            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemTransporteSabado", for: indexPath) as! CustomCollectionViewCell
//
//            cell.lblExitSabado.text = arrayTraslatesSabado[indexPath.row]
//            cell.tvBusesSabado.tag = indexPath.row + 10
//            cell.tvBusesSabado.isUserInteractionEnabled = false
//
//        }else{
//
//            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemDomingo", for: indexPath) as! CustomCollectionViewCell
//
//            cell.lblExitDomingo.text = arrayTraslatesSabado[indexPath.row]
//            cell.tvBusesDomingo.tag = indexPath.row + 20
//            cell.tvBusesDomingo.isUserInteractionEnabled = false
//
//        }
//
//
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        var size = CGSize()
//
//        if(collectionView == cvtransport){
//
//            size = CGSize(width: view.frame.width, height: 42 + (19 * CGFloat(arrayBuses[indexPath.row].count)))
//
//        }else if(collectionView == cvTransportSabado){
//
//            size = CGSize(width: view.frame.width, height: 42 + (19 * CGFloat(arrayBusesSabado[indexPath.row].count)))
//        }else{
//
//            size = CGSize(width: view.frame.width, height: 42 + (19 * CGFloat(arrayBusesDomingo[indexPath.row].count)))
//            print(size.height)
//        }
//
//        return size
//    }
//}

