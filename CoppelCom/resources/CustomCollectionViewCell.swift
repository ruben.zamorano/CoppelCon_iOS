//
//  CustomCollectionViewCell.swift
//  appConvencion
//
//  Created by MacBook Pro on 17/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconMenu: UIImageView!
    @IBOutlet weak var lblTitleMenu: UILabel!
    
    @IBOutlet weak var iconMenuTwo: UIImageView!
    
    @IBOutlet weak var lblTitleMenuTwo: UILabel!
    
    
    @IBOutlet weak var imgGalery: UIImageView!
    
    
    
    @IBOutlet weak var lblExit: UILabel!
    @IBOutlet weak var tvBuses: UITableView!
    
    
    
    @IBOutlet weak var lblExitSabado: UILabel!
    @IBOutlet weak var tvBusesSabado: UITableView!
    
    @IBOutlet weak var lblExitDomingo: UILabel!
    @IBOutlet weak var tvBusesDomingo: UITableView!
    
    
    @IBOutlet weak var tvRespuestas: UITableView!
    @IBOutlet weak var lblPregunta: UILabel!
    @IBOutlet weak var viewColor: UIView!
    
    
}
