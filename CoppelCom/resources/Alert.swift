//
//  Alert.swift
//  CoppelCom
//
//  Created by MacBook Pro on 23/11/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialSnackbar

class Alert {
    
    func normalAlert(tittle:String, msg:String, context:AnyObject){
     
        let alert = UIAlertController(title: tittle, message: msg, preferredStyle:.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        context.present(alert, animated: true, completion: nil)
        
    }
    
    static func showSimpleDialog(controller: UIViewController, title: String, message: String) {
        let dialogController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        dialogController.addAction(okButton)
        controller.present(dialogController, animated: true, completion: nil)
    }
    
    static func showProgressDialog(controller: UIViewController, title: String, message: String) -> UIAlertController {
        let progressDialogController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.present(progressDialogController, animated: true, completion: nil)
        return progressDialogController
    }
    
    static func showErrorAccessDialog(controller: UIViewController, title: String, message: String) {
        let errorDialogController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default) { (alertAction: UIAlertAction!) -> Void in
            UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
            exit(EXIT_SUCCESS)
        }
        errorDialogController.addAction(okButton)
        controller.present(errorDialogController, animated: true, completion: nil)
    }
    
}
