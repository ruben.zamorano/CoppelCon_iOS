//
//  JSONKey.swift
//  CoppelCom
//
//  Created by MacBook Pro on 16/11/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import Foundation

class JSONKey {
    
    static let server = "http://192.168.41.215/pruebas/ServiciosCoppelCon/"
    
    
//    static let URL_GET_AGENDA_DEV = "http://localhost/pruebas/ServiciosCoppelCon/GetAgenda.json"
//    static let URL_GET_HOTEL_DEV = "http://localhost/pruebas/ServiciosCoppelCon/GetHotel.json"
//    static let URL_GET_KIT_BIENVENIDA = "http://localhost/pruebas/ServiciosCoppelCon/GetKitBienvenida.json"
    //static let URL_GET_PONENTS = "http://localhost/pruebas/ServiciosCoppelCon/GetPonentes.json"
    static let URL_GET_PONENTS = "\(server)GetPonentes.json"
    //static let URL_GET_TRANSPORT = "http://localhost/pruebas/ServiciosCoppelCon/GetTraslado.json"
    static let URL_OPENWHEATHER = "http://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b1b15e88fa797225412429c1c50c122a1"
    //static let URL_GET_GIFTS = "http://localhost/pruebas/ServiciosCoppelCon/GetRegalo.json"

    static let URL_LOGIN = "http://35.225.6.89/admin/login"
    static let URL_ENCUESTA =  "http://35.225.6.89/admin/guardarencuesta"
    static let URL_IMAGE_BASE = "https://drive.google.com/uc?export=download&id="
    
    static let KEY_CON = "tipo_convencion"
    static let KEY_CON_NAC = "convencionNacional"
    static let KEY_CON_REG = "convencionRegional"
    static let KEY_ID_USER = "idUser"
    
    static let KEY_AGEND_dia = "cDia"
    static let KEY_AGEND_ARRAY_ITINERARIO = "lstItinerario"
    static let KEY_C_HORA = "cHora"
    static let KEY_C_TITULO = "cTitulo"
    static let KEY_C_DESCRIPCION = "cDescripcion"
    static let KEY_TIPODEPANTALLA = "nIdTipoPantalla"
    static let KEY_ID = "nIdItinerario"
    static let KEY_N_IDPONENTE = "nIdPonente"
    static let KEY_N_IDITINERARIO = "nIdItinerario"
    static let KEY_C_IMAGEN = "cImagen"
    
    
    
    static let KEY_C_NOMBRE_EMPLEADO = "cNombreEmpleado"
    static let KEY_C_NOOMBRE_HOTEL = "cNombreHotel"
    static let KEY_C_HABITACION = "cHabitacion"
    static let KEY_C_TIPO_HABITACION = "cTipoHabitacion"
    static let KEY_C_ENTRADA = "cEntrada"
    static let KEY_C_SALIDA = "cSalida"
    static let KEY_B_PAREJA = "bPareja"
    static let KEY_EMAIL = "emailUser"
    static let KEY_CODE_FOTO = "codigoFoto"
    static let KEY_PAREJAS = "Pareja"
    static let KEY_C_NOMBRES = "cNombres"
    static let KEY_C_PHOTO = "cPhoto"
    
    
    
    static let KEY_ID_PONENTE = "_id"
    static let KEY_C_NOMBREPONENTE = "cNombrePonente"
    static let KEY_C_TEMAPRESENTACION_PONENTE = "cTemaPresentacion"
    static let KEY_C_UBICACION = "cUbicacion"
    static let KEY_C_MESA = "cMesa"
    static let KEY_N_MESA = "nMesa"
    static let KEY_C_CODIGOVESTIMENTA = "cCodigoVestimenta"
    static let KEY_LST_PONENTES = "lstPonentes"
    
    static let KEY_C_CODIGO = "cCodigo"
    static let KEY_C_LUGAR = "cLugar"
    static let KEY_C_FOTO = "cFoto"
    
    
    static let KEY_C_DIA = "cDia"
    static let KEY_LST_TRASLADOS = "lstTraslados"
    static let KEY_LST_CAMIONES = "lstVehiculos"
    static let KEY_C_TRANSPORTE = "cTransporte"
    
    
    
    //Core DATA MARK-
    
    
    static let TB_NOTIFY = "Notify"
    
    static let KEY_MSG = "msg"
    static let KEY_ACTION = "action"
    static let KEY_STATUS = "status"
    static let KEY_TITLE = "title"
    static let KEY_NUMERO_MESA = "nMesa"
}

