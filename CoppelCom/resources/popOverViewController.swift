//
//  popOverViewController.swift
//  appConvencion
//
//  Created by MacBook Pro on 17/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit

class popOverViewController: UIViewController {

    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnChgCon: UIButton!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblCorreo: UILabel!
    
    let sessionUser = UserDefaults.standard
    
    var delegateBack:backDelegate?
    var salir:logoutDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblNombre.text = sessionUser.object(forKey: JSONKey.KEY_C_NOMBRE_EMPLEADO) as? String
        lblCorreo.text = sessionUser.object(forKey: JSONKey.KEY_EMAIL) as? String
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.width / 2
        imgUserProfile.clipsToBounds = true
        
        btnLogout.layer.borderColor = UIColor.white.cgColor
        btnLogout.layer.borderWidth = CGFloat(Float(1))
        
        
        btnChgCon.layer.borderColor = UIColor.white.cgColor
        btnChgCon.layer.borderWidth = CGFloat(Float(1))

    }

    @IBAction func actionLogOut(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
        dismiss(animated: true) {
            self.salir?.logOut()
        }
    }
    
    @IBAction func actionCambiar(_ sender: UIButton) {
        
        print("back1")
        
        dismiss(animated: true) {
            self.delegateBack?.anotherCon()
        }
       
    }
}
protocol backDelegate {
    func anotherCon()
}

protocol logoutDelegate {
    
    func logOut()
    
}
