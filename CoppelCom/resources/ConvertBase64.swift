//
//  ConvertBase64.swift
//  CoppelCom
//
//  Created by MacBook Pro on 28/11/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import Foundation
import UIKit

class ConvertBase64{
    
    static func EncodingBase64(image:UIImage) -> String
    {
        let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        let strBase64:String = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return strBase64
    }
    
    static func DecodingBase64(image:String) -> NSData
    {
        let dataDecoded:NSData = NSData(base64Encoded: image, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        
        return dataDecoded
    }
    static func DecodingURLPicture(image:String) -> NSData
    {
        
        var img = NSData()
        
        let url:NSURL = NSURL(string : image)!
        
        
        
        if(image != "")
        {
            
            if let imageData = NSData.init(contentsOf: url as URL)
            {
                img = imageData
                
            }
        }
        
        return img
        
    }
}

