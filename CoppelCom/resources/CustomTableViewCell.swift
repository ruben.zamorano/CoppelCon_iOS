//
//  CustomTableViewCell.swift
//  appConvencion
//
//  Created by MacBook Pro on 17/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
//    @IBOutlet weak var dateDiary: UILabel!
//    @IBOutlet weak var titleDiary: UILabel!
//    @IBOutlet weak var hourDiary: UILabel!
//
//
//    @IBOutlet weak var dateDiaryTwo: UILabel!
//    @IBOutlet weak var hourDiaryTwo: UILabel!
//    @IBOutlet weak var titleDiaryTwo: UILabel!
//
//    @IBOutlet weak var dateDiaryThree: UILabel!
//    @IBOutlet weak var hourDiaryThree: UILabel!
//    @IBOutlet weak var titleDiaryThree: UILabel!
//
    
    @IBOutlet weak var lbltitleNotification: UILabel!
    @IBOutlet weak var lblDateNotification: UILabel!
    @IBOutlet weak var pointView: UIView!
    
    
    
    @IBOutlet weak var imgPonent: UIImageView!
    
    @IBOutlet weak var lblNamePonent: UILabel!
    
    @IBOutlet weak var lblThemePonent: UILabel!
    
    
    @IBOutlet weak var imgUserQuestions: UIImageView!
    @IBOutlet weak var lblNameUserQuestions: UILabel!
    
    @IBOutlet weak var lblDateQuestion: UILabel!
    
    @IBOutlet weak var lblStateQuestion: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    
    
    @IBOutlet weak var lblGiftKit: UILabel!
    
    @IBOutlet weak var lblHora: UILabel!
    @IBOutlet weak var lblBus: UILabel!
    
    
    @IBOutlet weak var lblHoraSabado: UILabel!
    @IBOutlet weak var lblBusSabado: UILabel!
    
    
    @IBOutlet weak var lblHoraDomingo: UILabel!
    @IBOutlet weak var lblBusDomingo: UILabel!
    
    
    @IBOutlet weak var lblDestinoFlight: UILabel!
    @IBOutlet weak var lblAreoFlight: UILabel!
    @IBOutlet weak var lblHourFlight: UILabel!
    @IBOutlet weak var lblDateFlight: UILabel!
    
    @IBOutlet weak var lblDateBus: UILabel!
    @IBOutlet weak var lblDescBus: UILabel!
    @IBOutlet weak var lblLineBus: UILabel!
    @IBOutlet weak var lblHourBus: UILabel!
   
    
    
    @IBOutlet weak var lblDateCar: UILabel!
    @IBOutlet weak var lblDescriptionCar: UILabel!
    
    @IBOutlet weak var lblDestinityCar: UILabel!
    
    @IBOutlet weak var lblHourCar: UILabel!
    
    @IBOutlet weak var lblDateFlightSub: UILabel!
    @IBOutlet weak var lblDescFlightSub: UILabel!
    @IBOutlet weak var lblLineFlightSub: UILabel!
    @IBOutlet weak var lblHourFlightSub: UILabel!
    
    
    @IBOutlet weak var lblDateBusSub: UILabel!
    @IBOutlet weak var lblDescBusSub: UILabel!
    @IBOutlet weak var lblLineBusSub: UILabel!
    @IBOutlet weak var lblHourBusSub: UILabel!
    
    @IBOutlet weak var lblDescCarSub: UILabel!
    @IBOutlet weak var lblDateCarSub: UILabel!
    
    @IBOutlet weak var lblDestiCarSub: UILabel!
    
    
    @IBOutlet weak var imgPonentDetails: UIImageView!
    @IBOutlet weak var lblNamePonentDetails: UILabel!
    @IBOutlet weak var lblThemePonentDetails: UILabel!
    
    @IBOutlet weak var lblPreguntaQuick: UILabel!
    
    @IBOutlet weak var segmentedQuick: UISegmentedControl!
    @IBOutlet weak var viewColor: NSLayoutConstraint!
    
    
    
}
