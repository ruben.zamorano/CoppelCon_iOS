//
//  NotificationObject.swift
//  CoppelCom
//
//  Created by MacBook Pro on 23/11/17.
//  Copyright © 2017 Coppel. All rights reserved.
//

import Foundation

class NotificationObject{
    
    var title = ""
    var msg = ""
    var action = ""
    var status = false
}
