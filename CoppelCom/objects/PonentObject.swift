//
//  PonentObject.swift
//  appConvencion
//
//  Created by MacBook Pro on 18/11/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import Foundation
import UIKit

class PonentObject{
    
    var id = ""
    var nombre = ""
    var tema = ""
    var img = UIImage()
    var ubicacion = ""
    var hora = ""
    var mesa = ""
    var descripcion = ""
    var codigoVestimenta = ""
    var puesto = ""
}
